import { notFound } from "next/navigation";
import { getRequestConfig } from "next-intl/server";
import { locales } from "./src/app/lib/global";

export default getRequestConfig(async ({ locale }) => {
  // Validate that the incoming `locale` parameter is valid
  if (!locales.includes(locale)) notFound();
  return {
    messages: (
      await (locale === "ch"
        ? // When using Turbopack, this will enable HMR for `ch`
          import("./messages/ch.json")
        : import(`./messages/${locale}.json`))
    ).default,
  };
});
