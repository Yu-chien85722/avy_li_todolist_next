/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/pages/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/components/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/app/**/*.{js,ts,jsx,tsx,mdx}",
  ],
  darkMode: "class",
  theme: {
    extend: {
      backgroundImage: {
        "gradient-radial": "radial-gradient(var(--tw-gradient-stops))",
        "gradient-conic":
          "conic-gradient(from 180deg at 50% 50%, var(--tw-gradient-stops))",
        "gradient-95": "linear-gradient(95deg, var(--tw-gradient-stops))",
        "gradient-185": "linear-gradient(185.14deg, var(--tw-gradient-stops))",
        "gradient-147": "linear-gradient(147.5deg, var(--tw-gradient-stops))",
      },
      colors: {
        primary: {
          DEFAULT: "rgba(251, 185, 56, 1)",
          op10: "rgba(251, 185, 56, 0.1)",
          op20: "rgba(251, 185, 56, 0.2)",
          op30: "rgba(251, 185, 56, 0.3)",
          op40: "rgba(251, 185, 56, 0.4)",
          op50: "rgba(251, 185, 56, 0.5)",
          op60: "rgba(251, 185, 56, 0.6)",
          100: "rgba(253, 226, 175, 1)",
        },
        success: {
          DEFAULT: "rgba(158, 191, 88, 1)",
          op20: "rgba(158, 191, 88, 0.2)",
          op30: "rgba(158, 191, 88, 0.3)",
          op40: "rgba(158, 191, 88, 0.4)",
          op50: "rgba(158, 191, 88, 0.5)",
          op60: "rgba(158, 191, 88, 0.6)",
          op90: "rgba(158, 191, 88, 0.9)",
        },
        danger: {
          DEFAULT: "rgba(191, 88, 88, 1)",
          op20: "rgba(191, 88, 88, 0.2)",
          op30: "rgba(191, 88, 88, 0.3)",
          op40: "rgba(191, 88, 88, 0.4)",
          op50: "rgba(191, 88, 88, 0.5)",
          op60: "rgba(191, 88, 88, 0.6)",
          op90: "rgba(191, 88, 88, 0.9)",
        },
        white: {
          DEFAULT: "rgba(254, 254, 254, 1)",
          100: "rgba(247, 247, 247, 1)",
          200: "rgba(245, 246, 247, 1)",
          op50: "rgba(254, 254, 254, 0.5)",
        },
        black: {
          DEFAULT: "rgba(51, 51, 51, 1)",
          op40: "rgba(51, 51, 51, 0.4)",
          800: "rgba(31, 28, 22, 1)",
          900: "rgba(43, 43, 43, 1)",
        },
        grey: {
          DEFAULT: "rgba(153, 153, 153, 1)",
          100: "rgba(227, 227, 227, 1)",
          163: "rgba(163, 163, 163, 1)",
          102: "rgba(102, 102, 102, 1)",
          239: "rgba(239, 239, 239, 1)",
        },
      },
      spacing: {
        4.5: "1.125rem",
      },
      fontSize: {
        "3.5xl": ["2rem", "2.375rem"],
        clamp48:
          "clamp(2rem, calc(32px + 16 * ((100vw - 1440px) / 480)), 3rem)",
        clamp36:
          "clamp(1.5rem, calc(24px + 12 * ((100vw - 1440px) / 480)), 2.25rem)",
        clamp28:
          "clamp(1.25rem, calc(20px + 8 * ((100vw - 1440px) / 480)), 1.75rem)",
        clamp24:
          "clamp(1.125rem, calc(18px + 6 * ((100vw - 1440px) / 480)), 1.5rem)",
        clamp20:
          "clamp(1rem, calc(16px + 4 * ((100vw - 1440px) / 480)), 1.25rem)",
        clamp18:
          "clamp(0.875rem, calc(14px + 4 * ((100vw - 1440px) / 480)), 1.125rem)",
        clamp16:
          "clamp(0.75rem, calc(12px + 4 * ((100vw - 1440px) / 480)), 1rem)",
        clamp14:
          "clamp(0.75rem, calc(12px + 2 * ((100vw - 1440px) / 480)), 0.875rem)",
      },
      boxShadow: {
        DEFAULT: "0.25rem 0.5rem 0.75rem 0rem rgba(51, 51, 51, 0.1);",
      },
      screens: {
        "3xl": "1920px",
        // "2xl": { max: "1535px" },
        // xl: { max: "1279px" },
        // lg: { max: "1023px" },
        // md: { max: "767px" },
        // sm: { max: "639px" },
      },
    },
  },
  plugins: [],
};
