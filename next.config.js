// 交大宿舍 NEXTAUTH_URL "http://192.168.0.38:3000"
// 交大宿舍 BACKEND_API_BASE: "http://192.168.0.37:80"

/** @type {import('next').NextConfig} */
const withNextIntl = require("next-intl/plugin")(
  // This is the default (also the `src` folder is supported out of the box)
  "./i18n.ts",
);
const nextConfig = {
  distDir: "build",
  env: {
    NEXTAUTH_SECRET: "2Nml9Fz/QNvGeurydXxZOG/E1koT02pA4xvQNNzUp9k=",
    BACKEND_API_BASE: process.env.BACKEND_API_BASE || "http://192.168.0.37:80",
    NEXTAUTH_URL: process.env.NEXTAUTH_URL || "http://ivy-kelly.f.mooo.com",
    // NEXTAUTH_URL_INTERNAL:
    //   process.env.NEXTAUTH_URL_INTERNAL || "http://localhost", // add line
  },
  experimental: {
    serverActions: true,
  },
  images: {
    //https://nextjs.org/docs/messages/next-image-unconfigured-host
    remotePatterns: [
      {
        protocol: "https",
        hostname: "cc-prod.scene7.com",
        port: "",
        pathname: "/is/image/CCProdAuthor/**",
      },
      {
        protocol: "https",
        hostname: "lh3.googleusercontent.com",
      },
      {
        protocol: "https",
        hostname: "via.placeholder.com",
      },
    ],
  },
};

module.exports = withNextIntl(nextConfig);
