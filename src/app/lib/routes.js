import { BsCalendar4, BsBarChartSteps } from "react-icons/bs";
import { RxDashboard } from "react-icons/rx";
import { GoProjectRoadmap } from "react-icons/go";
import { AiOutlineUnorderedList } from "react-icons/ai";
const routes = [
  {
    name: "dashboard",
    icon: <RxDashboard className="text-clamp48" />,
    route: "/",
  },
  {
    name: "calendar",
    icon: <BsCalendar4 className="text-clamp48" />,
    route: "/calendar",
  },
  {
    name: "project",
    icon: <GoProjectRoadmap className="text-clamp48" />,
    route: "/project",
  },
  {
    name: "todolist",
    icon: <AiOutlineUnorderedList className="text-clamp48" />,
    route: "/toDoList",
  },
  {
    name: "ganttchart",
    icon: <BsBarChartSteps className="text-clamp48" />,
    route: "/gantt",
  },
];

export default routes;
