import { AlertStatus } from "../_component/alert/Alert";
import { notFound } from "next/navigation";

// intl setting
const locales = ["en", "ch"];
// The `pathnames` object holds pairs of internal
// and external paths, separated by locale.
const pathnames = {
  "/": "/",
  "/calendar": {
    en: "/calendar",
    ch: "/calendar",
  },
  "/project": {
    en: "/project",
    ch: "/project",
  },
  "/toDoList": {
    en: "/toDoList",
    ch: "/toDoList",
  },
  "/gantt": {
    en: "/gantt",
    ch: "/gantt",
  },
  "/project/[projectId]": {
    en: "/project/[projectId]",
    ch: "/project/[projectId]",
  },
};
const localePrefix = "always";

const REG_EXP_PROJECTNAME =
  /^(?=.*[0-9a-zA-Z\u4E00-\u9FA5])[0-9a-zA-Z\u4E00-\u9FA5-\.\s]{1,20}$/;

// error: error message
// target: 無法完成的事情與動作，ex: save memo, get todoList...etc
const fetchDataErrHandler = (error, target) => {
  // console.log("err", error);
  // console.log("target", target);
  const errorType = error.message.split(":")[0];
  const errorMessage = error.message.split(":")[1];

  if (errorType === "ConnectError") {
    if (errorMessage === "404") {
      // console.log(404);
      return notFound();
    }
    AlertStatus("error", `Connect error, http status code: ${errorMessage}`);
  } else if (errorType === "UnauthorizedError") {
    AlertStatus(
      "error",
      "Unauthorized error, you do not have permission to process the data.",
    );
    throw "Unauthorized";
  } else if (errorType === "Forbidden") {
    AlertStatus(
      "error",
      "Forbidden error, you do not have permission to process the data.",
    );
    throw "Forbidden";
  } else {
    AlertStatus("error", `Can not ${target}, error message: ${errorMessage}`);
  }
};

const isDateInPeriod = (checkDate, startDate, endDate) => {
  if (
    new Date(checkDate).getTime() >= new Date(startDate).getTime() &&
    new Date(checkDate).getTime() <= new Date(endDate).getTime()
  ) {
    return true;
  }
  return false;
};

// 2024-03-05 to 05 March 2024 ---> en-US
const formatDate = (
  date,
  locales = "en-US",
  options = { day: "numeric", month: "long", year: "numeric" },
) => {
  // formate date and remove ,
  const formattedDate = new Intl.DateTimeFormat(locales, options)
    .format(new Date(date))
    .replace(/,/g, "");
  return formattedDate;
};

// const useGetCurLan = () => {
//   let curLang = "ch";
//   const pathnameArr = usePathname().split("/");
//   if (locales.includes(pathnameArr[1])) {
//     curLang = pathnameArr[1];
//   }
//   return curLang;
// };

const deepCopy = (obj) => {
  // 檢查 obj 是否是物件
  if (typeof obj !== "object" || obj === null) {
    return obj; // 如果不是物件，直接返回該值
  }

  // 創建一個新的物件或陣列
  const newObj = Array.isArray(obj) ? [] : {};

  // 對物件的每個屬性進行遞迴深層複製
  for (let key in obj) {
    // 檢查物件是否具有自己的屬性，而不是繼承來的
    if (obj.hasOwnProperty(key)) {
      // 如果屬性是物件，則遞迴進行深層複製
      newObj[key] = deepCopy(obj[key]);
    }
  }

  return newObj;
};

function debounce(fn, delay) {
  let timeout;
  return function (...args) {
    clearTimeout(timeout);
    timeout = setTimeout(() => {
      console.log("store item list");
      fn.apply(this, args);
    }, delay);
  };
}

export {
  locales,
  pathnames,
  localePrefix,
  fetchDataErrHandler,
  REG_EXP_PROJECTNAME,
  isDateInPeriod,
  formatDate,
  deepCopy,
  debounce,
};
