export async function getData(url, token) {
  console.log("getData", token);
  const res = await fetch(`${process.env.BACKEND_API_BASE}/api/gauth/${url}`, {
    method: "GET",
    headers: {
      Authorization: `Bearer ${token}`,
      Accept: "application/json",
      "X-Requested-With": "XMLHttpRequest",
    },
    next: { revalidate: 0 },
  });
  // const res = await fetch(`${process.env.BACKEND_API_BASE}/api/${url}`, {
  //   method: "GET",
  //   headers: {
  //     Authorization: `Bearer ${token}`,
  //     Accept: "application/json",
  //     "X-Requested-With": "XMLHttpRequest",
  //   },
  //   next: { revalidate: 0 },
  // });
  if (!res.ok) {
    if (res.status === 401) {
      throw new Error(`UnauthorizedError:${res.status}`);
    } else if (res.status === 403) {
      throw new Error(`Forbidden: ${res.status}`);
    } else {
      throw new Error(`ConnectError:${res.status}`);
    }
  }
  const resJson = await res.json();

  if (!resJson.status) {
    throw new Error(`Error:${resJson.ret}`);
  }

  return resJson.ret;
}

export async function putData(url, token, putData) {
  console.log("putData", token);
  console.log("putData", putData);
  const res = await fetch(`${process.env.BACKEND_API_BASE}/api/gauth/${url}`, {
    method: "PUT",
    headers: {
      Authorization: `Bearer ${token}`,
      Accept: "application/json",
      "X-Requested-With": "XMLHttpRequest",
      "Content-Type": "application/json",
    },
    next: { revalidate: 0 },
    body: putData,
  });
  // const res = await fetch(`${process.env.BACKEND_API_BASE}/api/${url}`, {
  //   method: "PUT",
  //   headers: {
  //     Authorization: `Bearer ${token}`,
  //     Accept: "application/json",
  //     "X-Requested-With": "XMLHttpRequest",
  //     "Content-Type": "application/json",
  //   },
  //   next: { revalidate: 0 },
  //   body: putData,
  // });
  if (!res.ok) {
    if (res.status === 401) {
      throw new Error(`UnauthorizedError:${res.status}`);
    } else if (res.status === 403) {
      throw new Error(`Forbidden: ${res.status}`);
    } else {
      throw new Error(`ConnectError:${res.status}`);
    }
  }
  const resJson = await res.json();

  if (!resJson.status) {
    throw new Error(`Error:${resJson.ret}`);
  }

  return resJson.ret;
}

export async function postData(url, token, postData) {
  console.log("postData", token);
  console.log("postData", postData);
  const res = await fetch(`${process.env.BACKEND_API_BASE}/api/gauth/${url}`, {
    method: "POST",
    headers: {
      Authorization: `Bearer ${token}`,
      Accept: "application/json",
      "X-Requested-With": "XMLHttpRequest",
      "Content-Type": "application/json",
    },
    next: { revalidate: 0 },
    body: postData,
  });
  // const res = await fetch(`${process.env.BACKEND_API_BASE}/api/${url}`, {
  //   method: "POST",
  //   headers: {
  //     Authorization: `Bearer ${token}`,
  //     Accept: "application/json",
  //     "X-Requested-With": "XMLHttpRequest",
  //     "Content-Type": "application/json",
  //   },
  //   next: { revalidate: 0 },
  //   body: postData,
  // });
  if (!res.ok) {
    if (res.status === 401) {
      throw new Error(`UnauthorizedError:${res.status}`);
    } else if (res.status === 403) {
      throw new Error(`Forbidden: ${res.status}`);
    } else {
      throw new Error(`ConnectError:${res.status}`);
    }
  }
  const resJson = await res.json();

  if (!resJson.status) {
    throw new Error(`Error:${resJson.ret}`);
  }

  return resJson.ret;
}

export async function delData(url, token) {
  console.log("delData", token);
  const res = await fetch(`${process.env.BACKEND_API_BASE}/api/gauth/${url}`, {
    method: "DELETE",
    headers: {
      Authorization: `Bearer ${token}`,
      Accept: "application/json",
      "X-Requested-With": "XMLHttpRequest",
      "Content-Type": "application/json",
    },
    next: { revalidate: 0 },
  });
  // const res = await fetch(`${process.env.BACKEND_API_BASE}/api/${url}`, {
  //   method: "DELETE",
  //   headers: {
  //     Authorization: `Bearer ${token}`,
  //     Accept: "application/json",
  //     "X-Requested-With": "XMLHttpRequest",
  //     "Content-Type": "application/json",
  //   },
  //   next: { revalidate: 0 },
  // });
  if (!res.ok) {
    if (res.status === 401) {
      throw new Error(`UnauthorizedError:${res.status}`);
    } else if (res.status === 403) {
      throw new Error(`Forbidden: ${res.status}`);
    } else {
      throw new Error(`ConnectError:${res.status}`);
    }
  }
  const resJson = await res.json();

  if (!resJson.status) {
    throw new Error(`Error:${resJson.ret}`);
  }

  return resJson.ret;
}
