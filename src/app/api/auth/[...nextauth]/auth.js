import CredentialsProvider from "next-auth/providers/credentials";
import GoogleProvider from "next-auth/providers/google";

/**
 * Takes a token, and returns a new token with updated
 * `accessToken` and `accessTokenExpires`. If an error occurs,
 * returns the old token and an error property
 */
// async function refreshAccessToken(token) {
//   console.log("refresh token start, old token: ", token.accessToken);
//   try {
//     // refresh token's api
//     const res = await fetch(
//       `${process.env.BACKEND_API_BASE}/api/refreshToken`,
//       {
//         method: "GET",
//         headers: {
//           Authorization: `Bearer ${token.accessToken}`,
//         },
//         next: { revalidate: 0 },
//       },
//     );

//     // 如 res.ok 不等於 true，將 res 的 status 丟出去 => 終止 try 的涵式，執行 catch 涵式
//     if (!res.ok) {
//       throw new Error(res.status);
//     }

//     const refreshedToken = await res.json();
//     if (!refreshedToken.status) {
//       throw new Error("can not refresh token.");
//     }
//     console.log("refresh token success, new token: ", refreshedToken.token);
//     return {
//       ...token,
//       accessToken: refreshedToken.token,
//       accessTokenExpires: Date.now() + refreshedToken.expSec * 1000,
//     };
//   } catch (error) {
//     console.log("refreshToken Err", error);
//     return {
//       ...token,
//       error: "RefreshAccessTokenError",
//     };
//   }
// }

async function refreshAccessToken(token) {
  console.log("refreshAccessToken token~~~~~~~~: ", token);
  // token = token.token;
  try {
    const url =
      "https://www.googleapis.com/oauth2/v4/token?" +
      new URLSearchParams({
        client_id: process.env.GOOGLE_CLIENT_ID,
        client_secret: process.env.GOOGLE_CLIENT_SECRET,
        grant_type: "refresh_token",
        refresh_token: token.token.refresh_token ?? token.account.refresh_token,
      });

    const response = await fetch(url, {
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
        Accept: "application/json",
      },
      // body: JSON.stringify({
      //   client_id: process.env.GOOGLE_CLIENT_ID,
      //   client_secret: process.env.GOOGLE_CLIENT_SECRET,
      //   grant_type: "refresh_token",
      //   refresh_token: token.refreshToken,
      // }),
      method: "POST",
    });

    const refreshedTokens = await response.json();

    if (!response.ok) {
      console.log("refresh token error~~~~~~~~: ", refreshedTokens);
      throw refreshedTokens;
    }

    console.log("refresh token refreshedToken~~~: ", refreshedTokens);
    token.account.refreshToken =
      refreshedTokens.refresh_token ?? token.account.refreshToken;
    return {
      ...token,
      accessToken: refreshedTokens.access_token,
      accessTokenExpires: Date.now() + refreshedTokens.expires_in * 1000,
      refreshToken: refreshedTokens.refresh_token ?? token.account.refreshToken, // Fall back to old refresh token
      refreshAccessToken_FLAG: true,
    };
  } catch (error) {
    console.log(error);

    return {
      ...token,
      error: "RefreshAccessTokenError",
    };
  }
}

export const authOptions = {
  // 用來為 token 加密的一串文字，token 會被加密後放到 cookie 中
  secret: process.env.NEXTAUTH_SECRET,
  session: {
    jwt: true,
  },
  pages: {
    signIn: "/signIn",
  },
  providers: [
    CredentialsProvider({
      name: "Credentials",
      credentials: {
        email: {
          label: "email",
          type: "email",
          placeholder: "example@email.com",
        },
        password: {
          label: "password",
          type: "password",
        },
      },
      async authorize(credentials) {
        const res = await fetch(`${process.env.BACKEND_API_BASE}/api/login`, {
          method: "POST",
          body: JSON.stringify(credentials),
          headers: { "Content-Type": "application/json" },
          next: { revalidate: 0 },
        });
        const user = await res.json();
        if (res.ok && user.status) {
          return user;
        }
        // Return null if user data could not be retrieved
        return null;
      },
    }),
    GoogleProvider({
      clientId: process.env.GOOGLE_CLIENT_ID,
      clientSecret: process.env.GOOGLE_CLIENT_SECRET,
      authorization: {
        params: {
          prompt: "consent",
          access_type: "offline",
          response_type: "code",
          scope: "profile email openid",
        },
      },
    }),
  ],
  callbacks: {
    // async jwt({ token, user }) {
    //   // initial sign in 第一次登入時只有 user 可被取得，往後取得 session 皆使用 "token"
    //   if (user) {
    //     // console.log("init");
    //     return {
    //       accessToken: user.token,
    //       accessTokenExpires: Date.now() + user.expSec * 1000,
    //       user,
    //     };
    //   }
    //   console.log("token", token);
    //   // console.log(token.accessTokenExpires ?? undefined);
    //   // Return previous token if the access token has not expired yet
    //   if (Date.now() < token.accessTokenExpires) {
    //     return token;
    //   }
    //   // Access token has expired, try to update it
    //   return refreshAccessToken(token);
    // },
    // async session({ session, token }) {
    //   if (token) {
    //     session.accessToken = token.accessToken;
    //     session.error = token.error ?? null;
    //   }
    //   return session;
    // },

    // https://next-auth.js.org/configuration/callbacks#jwt-callback
    // jwt1
    async jwt1({ token, account, profile }) {
      // Persist the OAuth access_token and or the user id to the token right after signin
      if (account) {
        token.accessToken = account.access_token;
        token.id = profile.id;
      }
      return token;
    },

    //https://next-auth.js.org/v3/tutorials/refresh-token-rotation
    async jwt(token, account, profile) {
      var tokenRE;
      var usedToken;
      console.log("jwt~~~~~~~~~~~~~~: ", token);
      // check if token has the attribute "session" or not
      if ("session" in token) {
        console.log("token.session~~~~~~~~~~~~~~: ", token.session);
        usedToken = token.token;
      } else {
        console.log("no session~~~~~~~~~~~~~~: ");
        usedToken = token;
      }

      // Initial sign in
      // Return previous token if the access token has not expired yet

      usedToken.accessTokenExpires = usedToken.token.expires_at * 1000;
      if (Date.now() < usedToken.token.accessTokenExpires) {
        if (account) {
          usedToken.accessToken = account.access_token;
          usedToken.id = profile.id;
          usedToken.refreshToken = account.refresh_token;
          usedToken.accessTokenExpires = account.expires_at * 1000;
        }
        return usedToken;
      }

      // Access token has expired, try to update it
      tokenRE = refreshAccessToken(usedToken);
      console.log("jwt tokenRE~~~~~~~~: ", tokenRE);
      return tokenRE;
    },
    async session({ session, token }) {
      console.log("session~~~~~~~~~~", session);
      console.log("token~~~~~~~~~~~~", token);
      if (token) {
        session.user = token.user;
        session.accessToken = token.accessToken;
        session.error = token.error;
      }
      return session;
    },
  },
};
