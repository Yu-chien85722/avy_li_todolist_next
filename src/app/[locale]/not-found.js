"use client";

import Image from "next/legacy/image";
import notFoundImg from "@/app/images/404.png";

export default function NotFound() {
  return (
    <div className="flex h-full w-full items-center justify-center">
      <Image src={notFoundImg} alt="404 Error" priority />
    </div>
  );
}
