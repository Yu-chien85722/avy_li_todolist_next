"use client";
import Button from "@/app/_component/Button";
import { HiPlus } from "react-icons/hi";
import { IoWarningOutline } from "react-icons/io5";
import { delData, getData, postData, putData } from "@/app/api";
import useSWR from "swr";
import React, { useCallback } from "react";
import { fetchDataErrHandler } from "@/app/lib/global";
import { useState } from "react";
import ProjectCard from "@/app/_component/ProjectCard";
import { useTranslations } from "next-intl";
import { useRouter } from "@/navigation";
import ProjectModal from "@/app/_component/ProjectModal";
import Modal from "@/app/_component/Modal";
import Loading from "@/app/[locale]/loading";
import FailLoad from "@/app/_component/FailLoad";
import UserAuthHOC from "@/app/_component/HOC/UserAuthHOC";

function ProjectPage({ session, status }) {
  // router
  const router = useRouter();
  // intel
  const t = useTranslations("ProjectModal");
  const acBtnT = useTranslations("ActionBtn");
  // get projects
  const {
    data: projects,
    error,
    isLoading,
    mutate,
  } = useSWR(
    session ? ["project/progress", session.accessToken] : null,
    ([url, token]) => getData(url, token),
  );
  // modal open for create, update, false
  const [open, setOpen] = useState(false);
  const [projectId, setProjectId] = useState(null);
  // modal open for delete, false
  const [openDel, setOpenDel] = useState(false);

  const postToBE = (newProjectData) => {
    console.log("post to BE", newProjectData);
    let operationsObj = {
      create: {
        func: postData,
        url: "project",
        errMsg: "creat new project",
      },
      update: {
        func: putData,
        url: `project/${projectId}`,
        errMsg: "update project",
      },
    };
    const operation = operationsObj[open];
    operation
      .func(operation.url, session.accessToken, JSON.stringify(newProjectData))
      .then((result) => {
        console.log(result);
        if (result) {
          mutate();
        }
      })
      .catch((error) => {
        fetchDataErrHandler(error, operation.errMsg);
      });
  };

  // open edit or create project modal
  const onEditProject = useCallback((e) => {
    // console.log("project id", e.target.dataset.id);
    setOpen("update");
    setProjectId(e.target.dataset.id);
  }, []);
  // open del project modal
  const onDelProject = useCallback((e) => {
    // console.log("project id", e.target.dataset.id);
    setProjectId(e.target.dataset.id);
    setOpenDel(true);
  }, []);

  // post del project to BE
  const delProject = () => {
    setOpenDel(false);
    delData(`project/${projectId}`, session.accessToken)
      .then((result) => {
        console.log(result);
        if (result.length > 0) {
          mutate();
        }
      })
      .catch((error) => {
        fetchDataErrHandler(error, "delete project");
      });
  };

  if (status === "loading" || isLoading) {
    return <Loading />;
  }

  if (error) {
    fetchDataErrHandler(error, "load projects");
    return <FailLoad />;
  }

  // console.log(projects);
  return (
    <main className="h-full min-h-screen w-full xl:px-7 xl:py-8 3xl:px-10 3xl:py-12">
      <div className="flex h-full w-max flex-col flex-wrap items-start justify-start xl:gap-6 3xl:gap-9">
        <Button
          title={{
            text: t("modalTitle", { operation: "create" }),
            className:
              "create-btn xl:w-[18.75rem] 2xl:w-[23.438rem] 3xl:w-[28.125rem]",
          }}
          clickHandler={() => {
            setOpen("create");
          }}
        >
          <div className="flex items-center justify-center rounded-full bg-primary xl:h-9 xl:w-9 2xl:h-10 2xl:w-10 3xl:h-[3.125rem] 3xl:w-[3.125rem]">
            <HiPlus className="fill-white text-clamp20" />
          </div>
        </Button>
        {projects.length > 0 &&
          projects.map((project) => {
            return (
              <div
                className="cursor-pointer rounded-2xl bg-white shadow dark:bg-black xl:w-[18.75rem] 2xl:w-[23.438rem] 3xl:w-[28.125rem]"
                key={project.id}
                onClick={() => {
                  router.push(`/project/${project.id}`);
                }}
              >
                <ProjectCard
                  project={project}
                  showFlag={true}
                  operations={{ onEditProject, onDelProject }}
                />
              </div>
            );
          })}
      </div>
      <ProjectModal
        openType={open}
        projectId={projectId}
        postFormData={postToBE}
        setOpen={setOpen}
      />
      <Modal
        open={openDel}
        onClose={() => {
          setOpenDel(false);
        }}
        title={t("modalTitle", { operation: "delete" })}
      >
        <div className="mx-auto flex flex-col items-center justify-center xl:w-[28.125rem] xl:gap-9 xl:py-6 3xl:w-[37.5rem] 3xl:gap-12 3xl:py-9">
          <div className="text-center">
            <div className="mb-4 flex items-center justify-center">
              <IoWarningOutline className="h-[3.75rem] w-[3.75rem] text-danger" />
            </div>
            <div className="text-clamp24">
              {t.rich("delDescrip", {
                p: (chunks) => <p>{chunks}</p>,
              })}
            </div>
          </div>
          <div className="flex gap-6">
            <button
              className="btn-border"
              onClick={() => {
                setOpenDel(false);
              }}
            >
              {acBtnT("cancel")}
            </button>
            <button className="btn-bg-full" onClick={delProject}>
              {acBtnT("save")}
            </button>
          </div>
        </div>
      </Modal>
    </main>
  );
}

export default UserAuthHOC(ProjectPage);
