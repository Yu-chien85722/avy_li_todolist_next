"use client";
import React, { useMemo } from "react";
import useSWR from "swr";
import { useState, useEffect, useRef } from "react";
import { delData, getData, postData, putData } from "@/app/api";
import { fetchDataErrHandler } from "@/app/lib/global";
import { useTranslations } from "next-intl";
import { FiFlag } from "react-icons/fi";
import cx from "classnames";
import ItemList from "@/app/_component/ItemList";
import EllipseDropdown from "@/app/_component/EllipseDropdown";
import MissionModal from "@/app/_component/MissionModal";
import Modal from "@/app/_component/Modal";
import MyGantt from "@/app/_component/MyGantt";
import { IoWarningOutline } from "react-icons/io5";
import Loading from "@/app/[locale]/loading";
import FailLoad from "@/app/_component/FailLoad";
import UserAuthHOC from "@/app/_component/HOC/UserAuthHOC";

function SingleProjectPage({ params: { projectId }, session, status }) {
  // intl
  const t = useTranslations("SingleProjectPage");
  const acBtnT = useTranslations("ActionBtn");
  const missionModalT = useTranslations("MissionModal");
  // get missions
  const {
    data: projectData,
    error: missionErr,
    isLoading: missionLoading,
    mutate: missionMutate,
  } = useSWR(
    session ? [`project/${projectId}`, session.accessToken] : null,
    ([url, token]) => getData(url, token),
  );
  // mission modal open for create, update, false
  const [open, setOpen] = useState(false);
  // pickMission
  const [pickMission, setPickMission] = useState({});
  // modal open for delete, false
  const [openDel, setOpenDel] = useState(false);
  // actions data
  const [actionData, setActionData] = useState(null);
  // save btn show or not
  const [showSaveBtn, setShowSaveBtn] = useState(false);

  // create or update a mission
  const postToBE = (newMissionData) => {
    const postMData = {
      ...newMissionData,
      root: projectId,
      parent: projectId,
    };
    // console.log("post to BE", postMData);
    let operationsObj = {
      create: {
        func: postData,
        url: "project/mission",
        errMsg: "creat new mission",
      },
      update: {
        func: putData,
        url: `project/mission/${pickMission.id}`,
        errMsg: "update mission",
      },
    };
    const operation = operationsObj[open];
    operation
      .func(operation.url, session.accessToken, JSON.stringify(postMData))
      .then((result) => {
        // console.log(result);
        if (result) {
          missionMutate();
        }
      })
      .catch((error) => {
        fetchDataErrHandler(error, operation.errMsg);
      });
  };

  // open edit or create mission modal
  const onEditMission = (e) => {
    setOpen("update");
  };
  // open del mission modal
  const onDelMission = (e) => {
    // console.log("mission id", e.target.dataset.id);
    setOpenDel(true);
  };

  // post del mission to BE
  const delMission = () => {
    setOpenDel(false);
    delData(`project/mission/${pickMission.id}`, session.accessToken)
      .then((result) => {
        // console.log(result);
        if (result.length > 0) {
          missionMutate();
          setPickMission({});
        }
      })
      .catch((error) => {
        fetchDataErrHandler(error, "delete mission");
      });
  };

  // update specific mission's actions
  const storeHandler = (itemArr) => {
    // console.log("result", itemArr);
    const updateActions = itemArr.map((action) => {
      return {
        // begin_date:
        //   action.begin_date ?? moment(new Date()).format("YYYY-MM-DD"),
        // end_date: action.end_date ?? moment(new Date()).format("YYYY-MM-DD"),
        name: action.text,
        id: action.id,
      };
    });
    // console.log("post result", updateActions);
    putData(
      `project/mission/${pickMission.id}/actions`,
      session.accessToken,
      JSON.stringify(updateActions),
    )
      .then((result) => {
        // console.log(result);
        setShowSaveBtn(false);
        for (let operate of ["create", "delete", "update"]) {
          if (result[operate].length > 0) {
            result[operate].forEach((action) => {
              if (!action.status) {
                // reset actions
                setActionData(null);
                getActions();
                throw new Error(`Error:${action.ret}`);
              } else {
                missionMutate();
              }
            });
          }
        }
      })
      .catch((error) => {
        fetchDataErrHandler(error, "save actions");
      });
  };

  // get specific mission's actions
  const getActions = () => {
    getData(`project/mission/${pickMission.id}`, session.accessToken)
      .then((result) => {
        result.actions.forEach((action) => {
          action.text = action.name;
          if (action["done_at"]) {
            action["done_at"] = action["done_at"].slice(0, 10);
          }
        });
        setActionData(result.actions);
      })
      .catch((error) => {
        fetchDataErrHandler(error, "get actions");
      });
  };

  // pick specifc mission
  const onPickMission = (missionId) => {
    // console.log("pick_missions", projectData.actions);
    const mission = projectData.actions.find(
      (mission) => mission.id === missionId,
    );
    // console.log("pick mission", mission);
    setPickMission(mission);
  };

  const displayOption = useMemo(() => {
    return { columnWidth: "400", listCellWidth: "" };
  }, []);

  // get specific mission's actions
  useEffect(() => {
    if (pickMission.id) {
      setActionData(null);
      getActions();
    }
  }, [pickMission.id]);

  if (status === "loading" || missionLoading) {
    return <Loading />;
  }

  if (missionErr) {
    fetchDataErrHandler(missionErr, "load missions");
    return <FailLoad />;
  }

  // console.log("missions", projectData);

  let flagColor = "";
  switch (projectData.priority) {
    case 2:
      flagColor = "danger";
      break;
    case 1:
      flagColor = "primary";
      break;
    default:
      flagColor = "success";
  }

  return (
    <div className="flex h-full w-full flex-col items-center justify-center">
      <div className="flex h-3/6 w-full items-stretch justify-center xl:pb-6 2xl:pb-7 3xl:pb-9">
        <div className="mr-9 flex h-full flex-col items-start justify-start rounded-2xl bg-white px-6 py-6 shadow dark:bg-black xl:w-[calc(100%*0.7)] 3xl:w-4/6">
          <div className="flex items-center justify-start xl:mb-4 3xl:mb-6">
            <div className="me-1 text-clamp24 font-semibold">
              {projectData.name + " - " + t("listOfMission")}
            </div>
            <FiFlag
              className={`stroke-black text-clamp20 fill-${flagColor} me-3 dark:stroke-white`}
            />
            <div className="me-2 text-clamp16 text-grey">
              {t("startDate") + ": " + projectData["begin_date"].slice(0, 10)}
            </div>
            <div className="text-clamp16 text-grey">
              {t("endDate") + ": " + projectData["end_date"].slice(0, 10)}
            </div>
          </div>
          <div className="flex w-full grow flex-col flex-wrap items-start justify-start overflow-auto xl:gap-x-4 xl:gap-y-3 3xl:gap-x-6 3xl:gap-y-4">
            {projectData.actions.length > 0 &&
              projectData.actions.map((action) => (
                <div
                  className={cx(
                    "3x:py-2 flex items-center justify-between rounded-2xl border border-grey xl:w-[16.875rem] xl:px-3 xl:py-1.5 2xl:w-[18.75rem] 3xl:w-[21.5rem] 3xl:px-4",
                    {
                      "border-primary-100 bg-primary-100 dark:text-black":
                        pickMission.id === action.id,
                    },
                  )}
                  key={action.id}
                  onClick={() => {
                    onPickMission(action.id);
                  }}
                >
                  <div
                    className="me-1 truncate text-clamp20"
                    title={action.name}
                  >
                    {action.name}
                  </div>
                  <div
                    className={cx("whitespace-nowrap text-end text-clamp14", {
                      "text-black": pickMission.id === action.id,
                      "text-grey": pickMission.id !== action.id,
                    })}
                  >
                    <div className="">
                      {t("startDate") +
                        " : " +
                        action["begin_date"].slice(0, 10)}
                    </div>
                    <div className="">
                      {t("endDate") + " : " + action["end_date"].slice(0, 10)}
                    </div>
                  </div>
                </div>
              ))}
          </div>
          <div>
            <button
              className="text-clamp20 text-grey"
              onClick={() => {
                setOpen("create");
              }}
            >
              {"+ " + t("new")}
            </button>
          </div>
        </div>
        <div
          className={`relative flex h-full flex-col items-start justify-start rounded-2xl bg-white pb-1 pt-6 shadow dark:bg-black xl:w-[calc(100%*0.3)] xl:pl-4.5 3xl:w-2/6 3xl:pl-6 ${
            pickMission.id ? "visible" : "invisible"
          }`}
        >
          <EllipseDropdown>
            {/* dropdown menu */}
            <div
              id="operate-dropdownMenu"
              className="absolute right-4 top-8 z-10 w-36 rounded-lg border border-grey-163 bg-white dark:bg-black"
            >
              <div
                className="rounded-t-lg py-2 pl-4 text-sm hover:bg-primary-100 dark:hover:text-black"
                onClick={(e) => {
                  onEditMission(e);
                }}
              >
                {t("editMission")}
              </div>
              <div
                className="rounded-b-lg py-2 pl-4 text-sm hover:bg-primary-100 dark:hover:text-black"
                onClick={(e) => {
                  onDelMission(e);
                }}
              >
                {t("delMission")}
              </div>
            </div>
          </EllipseDropdown>
          <div className="text-clamp24 font-medium">
            {projectData.actions.find(
              (mission) => mission.id === pickMission.id,
            )?.name +
              " - " +
              t("listOfAction")}
          </div>
          <div className="relative w-full grow overflow-hidden pb-10 pr-5">
            {actionData && (
              <ItemList
                itemList={actionData}
                bulletColor={"bg-primary"}
                editable={true}
                storeHandler={storeHandler}
                showSaveBtn={showSaveBtn}
                setShowSaveBtn={setShowSaveBtn}
              />
            )}
          </div>
        </div>
      </div>
      <div className="project-gantt-container relative h-3/6 w-full rounded-2xl bg-white shadow dark:bg-black">
        <MyGantt
          showDone={true}
          showViewSwitcher={false}
          isDisabled={false}
          isHideChildren={false}
          displayOption={displayOption}
          getGanttAPI={`project/${projectId}/progress`}
          missionMutate={missionMutate}
          projectData={projectData}
        />
      </div>
      {/* create or update mission modal */}
      <MissionModal
        projectData={projectData}
        openType={open}
        missionId={pickMission.id}
        postFormData={postToBE}
        setOpen={setOpen}
      />
      {/* del mission modal */}
      <Modal
        open={openDel}
        onClose={() => {
          setOpenDel(false);
        }}
        title={missionModalT("modalTitle", { operation: "delete" })}
      >
        <div className="mx-auto flex flex-col items-center justify-center xl:w-[28.125rem] xl:gap-9 xl:py-6 3xl:w-[37.5rem] 3xl:gap-12 3xl:py-9">
          <div className="text-center">
            <div className="mb-4 flex items-center justify-center">
              <IoWarningOutline className="h-[3.75rem] w-[3.75rem] text-danger" />
            </div>
            <div className="text-clamp24">
              {missionModalT.rich("delDescrip", {
                p: (chunks) => <p>{chunks}</p>,
              })}
            </div>
          </div>
          <div className="flex gap-6">
            <button
              className="btn-border"
              onClick={() => {
                setOpenDel(false);
              }}
            >
              {acBtnT("cancel")}
            </button>
            <button className="btn-bg-full" onClick={delMission}>
              {acBtnT("save")}
            </button>
          </div>
        </div>
      </Modal>
    </div>
  );
}

export default UserAuthHOC(SingleProjectPage);
