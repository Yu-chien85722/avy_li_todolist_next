"use client";

import { useTranslations } from "next-intl";
import { Link } from "@/navigation";
import { AiOutlineLeft } from "react-icons/ai";

export default function SingleProjectLayout({ children }) {
  const t = useTranslations("SingleProjectPage");
  return (
    <div className="flex h-full grow flex-col items-start justify-center overflow-auto xl:pb-5 xl:pl-7 xl:pr-6 xl:pt-3.5 2xl:px-6 2xl:pb-9 3xl:pt-4.5">
      <Link
        className="flex items-center justify-start xl:mb-1 2xl:mb-4 3xl:mb-4.5"
        href={"/project"}
      >
        <div className="me-2">
          <AiOutlineLeft className="text-clamp24 text-grey" />
        </div>
        <div className="text-clamp24 text-grey">{t("project")}</div>
      </Link>
      <div className="w-full grow overflow-auto">{children}</div>
    </div>
  );
}

// export function generateStaticParams() {
//   console.log("get static params.");
//   const { data: session, status } = useSession();
//   const getProjectIdLIst = () => {
//     getData("project/progress", session.accessToken)
//       .then((result) => {
//         return result.map((project) => ({
//           projectId: project.id,
//         }));
//       })
//       .catch((err) => {
//         console.log(err);
//       });
//   };
//   useEffect(() => {
//     if (session?.error === "RefreshAccessTokenError") {
//       AlertStatus("error", "RefreshAccessTokenError");
//       signIn(); // Force sign in to hopefully resolve error
//     }
//   }, [session]);
//   useEffect(() => {
//     if (status === "authenticated") {
//       getProjectIdLIst();
//     }
//   }, [status]);
// }
