import { Inter } from "next/font/google";
import NextAuthProvider from "@/app/_provider/NextAuthProvider";
import Navigation from "@/app/_component/Navigation";
import LocaleSwitcher from "@/app/_component/LocaleSwitcher";
import { NextIntlClientProvider, useMessages } from "next-intl";
import { pick } from "lodash";
import { locales } from "@/app/lib/global";
import SignOutBtn from "@/app/_component/SignOutBtn";
import { unstable_setRequestLocale } from "next-intl/server";
import ThemeColorSwitcher from "../_component/ThemeColorSwitcher";
import ThemeColorProvider from "../_provider/ThemeColorProvider";

const inter = Inter({ subsets: ["latin"], display: "swap" });

export function generateStaticParams() {
  return locales.map((locale) => ({ locale }));
}

export const metadata = {
  title: "Ivy Tools",
  description: "Generated by create next app",
};

export default function LocaleLayout({ children, params: { locale } }) {
  // Enable static rendering
  unstable_setRequestLocale(locale);
  const messages = useMessages();
  return (
    <html lang={locale} suppressHydrationWarning>
      <body className={inter.className}>
        <ThemeColorProvider>
          <div className="flex h-full w-full">
            {/* side menu */}
            <div className="from-60.94% hidden h-full shrink-0 bg-gradient-185 from-primary-op30 via-success-op30 to-danger-op30 xl:block xl:px-4 xl:py-[2.375rem] 2xl:px-7 2xl:py-12 3xl:px-9 3xl:py-14">
              <div className="side-menu-box flex h-full flex-col items-center justify-between rounded-2xl bg-white dark:bg-black xl:pb-6 xl:pt-6 2xl:pb-8 2xl:pt-11 3xl:pb-10 3xl:pt-16">
                <NextIntlClientProvider
                  messages={pick(messages, ["Navigation", "LocaleSwitcher"])}
                  locale="ch"
                >
                  <Navigation />
                  <LocaleSwitcher />
                  <ThemeColorSwitcher />
                  <SignOutBtn />
                </NextIntlClientProvider>
              </div>
            </div>
            {/* main content */}
            <NextAuthProvider>
              <NextIntlClientProvider messages={messages} locale="ch">
                {children}
              </NextIntlClientProvider>
            </NextAuthProvider>
          </div>
        </ThemeColorProvider>
      </body>
    </html>
  );
}
