"use client";

import { useLocale, useTranslations } from "next-intl";
import React, { useEffect, useState } from "react";
import { getData, putData } from "@/app/api.js";
import { fetchDataErrHandler, formatDate } from "@/app/lib/global";
import moment from "moment";
import ItemList from "@/app/_component/ItemList";
import MyCalendar from "@/app/_component/MyCalendar";
import UserAuthHOC from "@/app/_component/HOC/UserAuthHOC";
import { BsCheckLg, BsFillCircleFill } from "react-icons/bs";
import { PiWarningCircleThin } from "react-icons/pi";
import Loading from "@/app/[locale]/loading";

function CalendarPage({ session, status }) {
  // get today
  const today = moment(new Date()).format("YYYY-MM-DD");
  // get current lang
  const curLang = useLocale();
  // intel
  const itemListT = useTranslations("ItemList");
  const t = useTranslations("CalendarPage");
  // states
  const [selectDate, setSelectDate] = useState(today);
  const [actionList, setActionList] = useState(null);
  const [memoList, setMemoList] = useState(null);
  const [noteActionId, setNoteActionId] = useState(null);
  const [noteList, setNoteList] = useState(null);
  const [memoSaveTime, setMemoSaveTime] = useState(0);
  // save button
  const [showSaveBtn, setShowSaveBtn] = useState(false);
  // note list container ref
  // const listContainerRef = useRef(null);

  const getThings = () => {
    // console.log(selectDate);
    // init => get value
    getData(
      `calendar/thing/${selectDate.replace(/-/g, "/")}`,
      session.accessToken,
    )
      .then((result) => {
        console.log("things", result);
        let actions = Object.values(result)[0]?.action ?? [];
        actions.forEach((action) => {
          action.text = action.name;
        });
        let memos = Object.values(result)[0]?.memo ?? [];
        if (memos.length > 0) {
          memos = memos.map((ele, index) => ({
            id: index + 1,
            text: ele,
          }));
        }
        console.log("actions", actions);
        console.log("memos", memos);
        setActionList(actions);
        setMemoList(memos);
      })
      .catch((error) => {
        fetchDataErrHandler(error, "get note list");
      });
  };

  const getNoteList = () => {
    // console.log("noteAciton id", noteActionId);
    // get done's note
    const actionObj = actionList.find((action) => action.id === noteActionId);
    let noteList = actionObj.comment ?? [];
    if (noteList.length > 0) {
      noteList = noteList.map((ele, index) => ({
        id: index + 1,
        text: ele,
      }));
    }
    setNoteList(noteList);
    // console.log("note list", noteList);
  };

  // check note
  const checkNoteHandler = (id) => {
    setNoteList(null);
    setNoteActionId(id);
  };

  // store memo list
  const storeHandler = (itemArr) => {
    console.log("memo list", itemArr);
    const updateMemoList = { memo: itemArr.map((item) => item.text) };
    console.log("memo list after", updateMemoList);
    putData(
      `calendar/thing/${selectDate}`,
      session.accessToken,
      JSON.stringify(updateMemoList),
    )
      .then((result) => {
        console.log(result);
        if (result) {
          setShowSaveBtn(false);
          setMemoSaveTime(new Date().getTime());
          // setMemoSaveTime((prevState) => prevState + 1);
        }
      })
      .catch((error) => {
        // if store error, sync BE's memo list to FE
        fetchDataErrHandler(error, "save memo");
      });
  };

  useEffect(() => {
    if (status === "authenticated") {
      if (selectDate) {
        getThings();
      }
    }
  }, [selectDate, status]);

  useEffect(() => {
    // init state
    setNoteActionId(null);
    setNoteList(null);
    setActionList(null);
    setMemoList(null);
    setShowSaveBtn(false);
  }, [selectDate]);

  useEffect(() => {
    if (noteActionId) {
      getNoteList();
    }
  }, [noteActionId]);

  if (status === "loading") {
    return <Loading />;
  }

  // console.log(memoSaveTime);
  return (
    <main className="flex h-full min-h-screen grow items-start justify-center overflow-hidden xl:gap-7 xl:px-6 xl:py-8 3xl:gap-9 3xl:px-[2.438rem] 3xl:py-11">
      <div className="h-full w-[calc(100%*0.64)] flex-auto">
        <div className="rbc-default-calendar relative h-full w-full bg-white dark:bg-black">
          <MyCalendar
            mode="calendar"
            selectDate={selectDate}
            setSelectDate={setSelectDate}
            memoSaveTime={memoSaveTime}
          />
          <div className="absolute bottom-4 right-9 flex items-center gap-4">
            <div className="flex items-center">
              <div className="group relative me-2 cursor-pointer">
                <PiWarningCircleThin className="fill-grey text-2xl" />
                <div className="invisible absolute z-10 flex items-center justify-start rounded border border-grey-239 bg-white text-clamp16 font-medium shadow group-hover:visible dark:border-grey dark:bg-black xl:px-3 xl:py-2 3xl:py-3">
                  <BsCheckLg className="me-1 fill-success text-2xl" />
                  <div className="whitespace-nowrap">
                    Ivy&emsp;
                    <span className="text-grey-102 dark:text-white">
                      {t("IvyDescrip")}
                    </span>
                  </div>
                </div>
              </div>
              <BsCheckLg className="fill-success text-2xl" />
              <span className="ms-1 text-base font-medium">
                {t("finished")}
              </span>
            </div>
            <div className="flex items-center">
              <BsFillCircleFill className="fill-primary text-xs" />
              <span className="ms-3 text-base font-medium">{t("memo")}</span>
            </div>
          </div>
        </div>
      </div>
      <div className="flex h-full w-3/12 flex-auto flex-col items-center justify-start">
        <div className="h-4/6 w-full shrink-0 rounded-2xl border bg-white shadow dark:bg-black xl:mb-7 3xl:mb-9">
          <div className="h-4/6 overflow-hidden border-b border-grey pt-6">
            <div className="done-title">
              <div>{itemListT("title.done")}</div>
              <div>
                {formatDate(selectDate, curLang === "ch" ? "zh-CN" : "en-US")}
              </div>
            </div>
            <div className="flex h-4/5 flex-col overflow-auto px-6 pb-2">
              {actionList && (
                <ItemList
                  itemList={actionList}
                  bulletColor="bg-success"
                  editable={false}
                  detailCb={checkNoteHandler}
                />
              )}
            </div>
          </div>
          <div className="mb-9 h-2/6 overflow-hidden px-6 pt-6">
            <div className="note-title">{itemListT("title.note")}</div>
            {noteList && (
              <div className="relative h-4/6 w-full">
                <ItemList
                  itemList={noteList}
                  bulletColor="bg-danger"
                  editable={false}
                />
              </div>
            )}
          </div>
        </div>
        <div className="relative w-full grow overflow-hidden rounded-2xl bg-white px-6 pt-6 shadow dark:bg-black">
          <div className="text-clamp24 font-medium">
            {itemListT("title.memo")}
          </div>
          {memoList && (
            <div className="h-4/6">
              <ItemList
                itemList={memoList}
                bulletColor="bg-primary"
                editable={true}
                storeHandler={storeHandler}
                showSaveBtn={showSaveBtn}
                setShowSaveBtn={setShowSaveBtn}
              />
            </div>
          )}
        </div>
      </div>
    </main>
  );
}

export default UserAuthHOC(CalendarPage);

// const dummyData2 = [
//   { id: 1, text: "官網首頁 RWD", note: true },
//   {
//     id: 2,
//     text: "盤點設變盤點設變盤點設變盤點設變盤點設變盤點設變盤點設變盤點設變盤點設變",
//     note: true,
//   },
//   { id: 3, text: "好市多買食材" },
//   { id: 4, text: "好市多買食材" },
//   { id: 5, text: "好市多買食材" },
//   { id: 6, text: "好市多買食材" },
// ];

// const dummyData = [
//   {
//     id: 1,
//     root: { name: "WMS" },
//     name: "入庫設變入庫設變入庫設變入庫設變入庫設變入庫設變入庫設變",
//     day: 300,
//   },
//   {
//     id: 2,
//     root: { name: "WMS" },
//     name: "入庫設變入庫設變入庫設變入庫設變入庫設變入庫設變入庫設變",
//     day: -2,
//   },
//   {
//     id: 3,
//     root: { name: "WMS" },
//     name: "入庫設變入庫設變入庫設變入庫設變入庫設變入庫設變入庫設變",
//     day: 0,
//   },
//   {
//     id: 4,
//     root: { name: "WMS" },
//     name: "入庫設變入庫設變入庫設變入庫設變入庫設變入庫設變入庫設變",
//     day: 300,
//   },
//   {
//     id: 5,
//     root: { name: "WMS" },
//     name: "入庫設變入庫設變入庫設變入庫設變入庫設變入庫設變入庫設變",
//     day: 300,
//   },
//   {
//     id: 6,
//     root: { name: "WMS" },
//     name: "入庫設變入庫設變入庫設變入庫設變入庫設變入庫設變入庫設變",
//     day: 300,
//   },
//   {
//     id: 7,
//     root: { name: "WMS" },
//     name: "入庫設變入庫設變入庫設變入庫設變入庫設變入庫設變入庫設變",
//     day: 300,
//   },
//   {
//     id: 8,
//     root: { name: "WMS" },
//     name: "入庫設變入庫設變入庫設變入庫設變入庫設變入庫設變入庫設變",
//     day: 300,
//   },
//   {
//     id: 9,
//     root: { name: "WMS" },
//     name: "入庫設變入庫設變入庫設變入庫設變入庫設變入庫設變入庫設變",
//     day: 300,
//   },
//   {
//     id: 10,
//     root: { name: "WMS" },
//     name: "入庫設變入庫設變入庫設變入庫設變入庫設變入庫設變入庫設變",
//     day: 300,
//   },
//   {
//     id: 11,
//     root: { name: "WMS" },
//     name: "入庫設變入庫設變入庫設變入庫設變入庫設變入庫設變入庫設變",
//     day: 300,
//   },
// ];
