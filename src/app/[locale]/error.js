"use client";

import { useEffect } from "react";
import Image from "next/legacy/image";
import unauthorizedImg from "@/app/images/401.png";
import forbiddenImg from "@/app/images/403.png";

export default function Error({ error, reset }) {
  // reset is a function,
  useEffect(() => {
    // Log the error to an error reporting service
    console.log(error);
    console.log(error.message);
  }, [error]);

  if (error === "Unauthorized") {
    return (
      <div className="flex h-full w-full items-center justify-center">
        <Image src={unauthorizedImg} alt="401 Error" priority />
      </div>
    );
  } else if (error === "Forbidden") {
    return (
      <div className="flex h-full w-full items-center justify-center">
        <Image src={forbiddenImg} alt="403 Error" priority />
      </div>
    );
  }
  return (
    <div>
      <h2>Something went wrong!</h2>
      <div>{JSON.stringify(error)}</div>
      <button onClick={() => reset()}>Try again</button>
    </div>
  );
}
