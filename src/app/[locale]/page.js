import MyCalendar from "../_component/MyCalendar";
import MyGantt from "../_component/MyGantt";
// client component
import ProjectList from "../_component/ProjectList";
import DashboardToDoList from "../_component/DashboardToDoList";
import DashboardMemo from "../_component/DashboardMemo";
import UserCard from "../_component/UserCard";
import { unstable_setRequestLocale } from "next-intl/server";

export default function Home({ params: { locale } }) {
  // Enable static rendering
  unstable_setRequestLocale(locale);
  return (
    <main className="flex h-full min-h-screen w-full items-start justify-start overflow-auto xl:gap-8 xl:px-6 xl:py-8 3xl:gap-9 3xl:px-9 3xl:py-10">
      <div className="flex h-full flex-col xl:w-[calc(100%*0.27)] xl:gap-7 2xl:w-[calc(100%*0.285)] 3xl:w-[calc(100%*0.3)] 3xl:gap-11">
        <div className="rbc-ds-calendar bg-white dark:bg-black xl:min-h-[16.3rem] 3xl:min-h-[24rem]">
          <MyCalendar mode="dsCalendar" />
        </div>
        <div className="grow overflow-y-auto pb-4 pr-2">
          <ProjectList />
        </div>
      </div>
      <div className="flex h-full flex-col xl:w-[calc(100%*0.73-2rem)] xl:gap-7 2xl:w-[calc(100%*0.715-2rem)] 3xl:w-[calc(100%*0.7-2.25rem)] 3xl:gap-9">
        <div className="flex grow items-start justify-start overflow-hidden xl:gap-7 3xl:gap-9">
          <div className="h-full w-[calc(100%*0.53)]">
            <DashboardToDoList />
          </div>
          <div className="flex h-full w-[calc(100%*0.47-2.25rem)] flex-col items-center justify-start xl:gap-8 2xl:gap-12 3xl:gap-16">
            <UserCard />
            <div className="w-full grow overflow-y-auto">
              <DashboardMemo />
            </div>
          </div>
        </div>
        <div className="dash-gantt-container relative h-[calc(100%*0.3)] w-full rounded-2xl bg-white shadow dark:bg-black">
          <MyGantt
            showDone={false}
            showViewSwitcher={false}
            isDisabled={true}
            isHideChildren={true}
            displayOption={{ columnWidth: "300", listCellWidth: "" }}
            getGanttAPI={"project/progress"}
            missionMutate={null}
            projectData={null}
          />
        </div>
      </div>
    </main>
  );
}
