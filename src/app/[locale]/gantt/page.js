import MyGantt from "../../_component/MyGantt";

export default function GanttPage() {
  return (
    <main className="h-full min-h-screen w-full overflow-auto xl:px-7 xl:py-8 3xl:px-10 3xl:py-12">
      <div className="relative h-full w-full rounded-2xl bg-white shadow dark:bg-black xl:pt-1.5 3xl:pt-9">
        <MyGantt
          showDone={true}
          showViewSwitcher={true}
          isDisabled={true}
          isHideChildren={false}
          displayOption={{ columnWidth: "200", listCellWidth: "18.75rem" }}
          getGanttAPI={"project/progress"}
          missionMutate={null}
          projectData={null}
        />
      </div>
    </main>
  );
}
