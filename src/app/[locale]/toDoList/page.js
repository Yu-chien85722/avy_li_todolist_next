"use client";

import useSWR from "swr";
import { useLocale, useTranslations } from "next-intl";
import ActionCard from "@/app/_component/ActionCard";
import ProjectCard from "@/app/_component/ProjectCard";
import ItemList from "@/app/_component/ItemList";
import { signIn, useSession } from "next-auth/react";
import React, { useEffect, useRef, useState, useMemo } from "react";
import { getData, putData } from "../../api";
import { AlertStatus } from "../../_component/alert/Alert";
import { fetchDataErrHandler, formatDate, deepCopy } from "@/app/lib/global";
import moment from "moment";
import { DragDropContext, Droppable, Draggable } from "react-beautiful-dnd";
import { LuTrash2 } from "react-icons/lu";
import Loading from "@/app/[locale]/loading";
import FailLoad from "@/app/_component/FailLoad";
import UserAuthHOC from "@/app/_component/HOC/UserAuthHOC";
import Accordion from "@/app/_component/Accordion";
import { AiOutlineEdit } from "react-icons/ai";

// get readyto and done list from BE
const useActions = () => {
  // console.log("useActionis");
  // get session for communicate with BE
  const { data: session, status } = useSession();
  const today = moment(new Date()).format("YYYY-MM-DD");
  // get tasks in the to do list
  // "2024-01-03"
  const {
    data: readytoData,
    error: readytoErr,
    isLoading: readytoLoading,
    mutate: readytoMute,
  } = useSWR(
    session ? [`todo/readyto/${today}`, session.accessToken] : null,
    ([url, token]) => getData(url, token),
  );
  // get done list on the right of the view
  // "2024-01-03"
  const {
    data: doneData,
    error: doneErr,
    isLoading: doneLoading,
    mutate: doneMute,
  } = useSWR(
    session ? [`todo/done/${today}`, session.accessToken] : null,
    ([url, token]) => getData(url, token),
  );

  useEffect(() => {
    if (session?.error === "RefreshAccessTokenError") {
      AlertStatus("error", "RefreshAccessTokenError");
      signIn(); // Force sign in to hopefully resolve error
    }
  }, [session]);
  // console.log("session loading", status);
  // console.log("readyto loading", readytoLoading);
  // console.log("done loading", doneLoading);

  const actionData = useMemo(() => {
    // if loading... or get error => send to main and don't sort out, empty obj
    if (status === "loading" || readytoLoading || doneLoading) {
      return {
        actions: {
          readyto: {
            items: [],
            readytoMute: null,
          },
          done: {
            items: [],
            doneMute: null,
          },
        },
        actionsLoading: true,
        actionsErr: null,
      };
    }
    if (readytoErr || doneErr) {
      return {
        actions: {
          readyto: {
            items: [],
            readytoMute: null,
          },
          done: {
            items: [],
            doneMute: null,
          },
        },
        actionsLoading: false,
        actionsErr: {
          readytoErr,
          doneErr,
        },
      };
    }

    // if sucess => loading false, error null,
    // 整理 toDoData and doneData
    readytoData.forEach((action) => {
      const diffDay = moment().diff(moment(action["end_date"]), "day");
      // console.log("diffDay", diffDay);
      action.day = diffDay;
    });
    doneData.forEach((action) => {
      const diffDay = moment().diff(moment(action["end_date"]), "day");
      // console.log("diffDay", diffDay);
      action.day = diffDay;
    });
    // 如果資料載入完成，返回從後端取得的 actions
    return {
      actions: {
        readyto: {
          items: readytoData,
          readytoMute,
        },
        done: {
          items: doneData,
          doneMute,
        },
      },
      actionsLoading: false,
      actionsErr: null,
    };
  }, [
    status,
    readytoLoading,
    readytoErr,
    doneLoading,
    doneErr,
    readytoData,
    doneData,
    readytoMute,
    doneMute,
  ]);

  return actionData;
};

const isExceptin = (sourceArea, destinationArea) => {
  if (destinationArea === "done" && sourceArea !== "readyto") {
    console.log("exception.");
    return true;
  }

  if (destinationArea === "shoulddo" && sourceArea !== "readyto") {
    console.log("exception.");
    return true;
  }

  return false;
};

function ToDoListPage({ session, status }) {
  // get today
  const today = moment(new Date()).format("YYYY-MM-DD");
  // get current lang
  const curLang = useLocale();
  // intel
  const toDoListT = useTranslations("ToDoList");
  const itemListT = useTranslations("ItemList");
  const acBtnT = useTranslations("ActionBtn");
  const actionCardT = useTranslations("ActionCard");
  // get project list and task list on the left side of the screen.
  const {
    data: projectData,
    error: projectErr,
    isLoading: projectLoading,
    mutate: projectMutate,
  } = useSWR(
    session ? ["project/progress", session.accessToken] : null,
    ([url, token]) => getData(url, token),
  );

  const {
    data: shouldDoData,
    error: shouldDoErr,
    isLoading: shouldDoLoading,
    mutate: shouldDoMutate,
  } = useSWR(
    session ? [`todo/shoulddo/${today}`, session.accessToken] : null,
    ([url, token]) => getData(url, token),
  );

  // states
  const { actions, actionsLoading, actionsErr } = useActions();
  const [noteActionId, setNoteActionId] = useState(null);
  const [noteList, setNoteList] = useState(null);
  const [actionObj, setActionObj] = useState(actions);

  // save button
  const [showSaveBtn, setShowSaveBtn] = useState(false);
  // note list container ref
  const listContainerRef = useRef(null);

  useEffect(() => {
    // console.log("list container", listContainerRef.current);
    if (listContainerRef.current) {
      listContainerRef.current.click();
    }
  }, [noteList]);
  // for readyto and done list
  useEffect(() => {
    setActionObj(actions);
  }, [actions]);

  // 當 noteAction item 不在 done list 時，clear noteList
  useEffect(() => {
    const doneList = actionObj.done.items;
    // console.log(typeof noteActionId);
    if (doneList.filter((action) => action.id === noteActionId).length === 0) {
      setNoteActionId(null);
      setNoteList(null);
    }
  }, [actionObj.done.items]);

  if (
    status === "loading" ||
    projectLoading ||
    shouldDoLoading ||
    actionsLoading
  ) {
    return <Loading />;
  }

  if (projectErr || shouldDoErr || actionsErr) {
    if (projectErr || shouldDoErr) {
      fetchDataErrHandler(projectErr ?? shouldDoErr, "load projects");
    } else if (actionsErr.readytoErr) {
      fetchDataErrHandler(actionsErr.readytoErr, "load to do list");
    } else if (actionsErr.doneErr) {
      fetchDataErrHandler(actionsErr.doneErr, "load done list");
    }
    return <FailLoad />;
  }

  // mark DONE
  const checkHandler = (id) => {
    // console.log(id);
    // put to backendar
    putData(
      `todo/done/${id}`,
      session.accessToken,
      JSON.stringify({ status: "Done" }),
    )
      .then((result) => {
        // sucess -> update to do list and done list
        // console.log("mark done success.");
        if (result) {
          // actionObj.readyto.readytoMute();
          // actionObj.done.doneMute();
          let newActionObj = deepCopy(actionObj);
          const doneIndex = actionObj.readyto.items.findIndex(
            (action) => action.id === id,
          );
          // console.log(doneIndex);
          let [removeAction] = newActionObj.readyto.items.splice(doneIndex, 1);
          newActionObj.done.items.splice(0, 0, removeAction);
          // console.log(newActionObj);
          setActionObj(newActionObj);
          projectMutate();
        }
      })
      .catch((error) => {
        // console.log(error);
        fetchDataErrHandler(error, "mark done");
      });
  };

  // edit DONE
  const editDoneHandler = (id) => {
    // console.log(id);
    setNoteActionId(id);
    // get done's memo
    getNoteList(id);
  };

  const getNoteList = (id) => {
    setNoteList(null);
    // `project/action/${id}`
    // `calendar/thing/${today.replace(/-/g, "/")}`;
    // `calendar/thing/2024/3/5`
    getData(`project/action/${id}`, session.accessToken)
      .then((result) => {
        // console.log("project action", result);
        let noteList = result.comment ?? [];
        if (noteList.length > 0) {
          noteList = noteList.map((ele, index) => ({
            id: index + 1,
            text: ele,
          }));
        }
        setNoteList(noteList);
      })
      .catch((error) => {
        fetchDataErrHandler(error, "get note list");
      });
  };

  // store Note list
  const storeHandler = (itemArr) => {
    // console.log("note list", itemArr);
    const updateNoteList = { comment: itemArr.map((item) => item.text) };
    console.log("note list after", updateNoteList);
    putData(
      `project/action/${noteActionId}`,
      session.accessToken,
      JSON.stringify(updateNoteList),
    )
      .then((result) => {
        console.log(result);
        if (result) {
          setShowSaveBtn(false);
        }
      })
      .catch((error) => {
        // if store error, sync BE's note list to FE
        fetchDataErrHandler(error, "save note");
        // init note list
        setNoteList(null);
        getNoteList(noteActionId);
      });
  };

  const dragEndHandler = (e) => {
    console.log(e);
    console.log("draggable ID", e.draggableId);

    const { draggableId, source, destination } = e;
    if (!destination) {
      return;
    }
    const destinationArea = destination.droppableId.split("-")[0];
    const sourceArea = source.droppableId.split("-")[0];
    // console.log("destination", destinationArea);
    // console.log("sourceArea", sourceArea);

    // if source and destination both === shoulddo or done => return
    if (isExceptin(sourceArea, destinationArea)) {
      return;
    }

    // copy new items (from state)
    let newActionObj = deepCopy(actionObj);
    let oriActionObj = deepCopy(actionObj);
    let removeAction;

    // if source area not in shoulddo area
    if (sourceArea === "shoulddo") {
      removeAction = shouldDoData.find((action) => {
        console.log("action in shoulddo data", action);
        return action.id === Number(draggableId);
      });
    } else {
      // splice(start, deleteCount, item )
      // 從 source 剪下被拖曳的元素
      removeAction = newActionObj[sourceArea].items.splice(source.index, 1)[0];
    }

    console.log("remove action", removeAction);
    // if destination area not in shoulddo area
    if (destinationArea !== "shoulddo") {
      // splice(start, deleteCount, item )
      // 在 destination 位置貼上被拖曳的元素
      newActionObj[destinationArea].items.splice(
        destination.index,
        0,
        removeAction,
      );
    }
    // console.log("new action obj", newActionObj);
    // console.log("ori action obj", oriActionObj);

    // update state
    setActionObj(newActionObj);
    // call API
    // success => do nothing
    // fail => set original
    switch (destinationArea) {
      case "done":
        putData(
          `todo/done/${draggableId}`,
          session.accessToken,
          JSON.stringify({ status: "Done" }),
        )
          .then((result) => {
            // success reload project => update project's progress
            if (result) {
              projectMutate();
            }
          })
          .catch((error) => {
            // if fail
            // set original state => draggled item return the original place
            setActionObj(oriActionObj);
            fetchDataErrHandler(error, "mark done");
          });
        break;
      // for trash icon in the todolist and left shoulddo list
      case "shoulddo":
        const newToDoIdList = newActionObj.readyto.items.map((item) => item.id);
        putData(
          `todo/readyto/${today}`,
          session.accessToken,
          JSON.stringify(newToDoIdList),
        )
          .then((result) => {
            // sucess -> update shouddo list
            if (result) {
              shouldDoMutate();
            }
          })
          .catch((error) => {
            // if fail
            // set original state => draggled item return the original place
            setActionObj(oriActionObj);
            fetchDataErrHandler(error, "change to do list");
          });
        break;
      case "readyto":
        if (sourceArea === "done") {
          putData(
            `todo/done/${draggableId}`,
            session.accessToken,
            JSON.stringify({ status: "Not Done" }),
          )
            .then((result) => {
              // success reload project => update project's progress
              if (result) {
                projectMutate();
              }
            })
            .catch((error) => {
              // if fail
              // set original state => draggled item return the original place
              setActionObj(oriActionObj);
              fetchDataErrHandler(error, "mark not done");
            });
        } else {
          const newToDoIdList = newActionObj.readyto.items.map(
            (item) => item.id,
          );
          putData(
            `todo/readyto/${today}`,
            session.accessToken,
            JSON.stringify(newToDoIdList),
          )
            .then((result) => {
              // sucess -> update shouddo list
              if (result) {
                if (sourceArea === "shoulddo") {
                  shouldDoMutate();
                }
              }
            })
            .catch((error) => {
              // if fail
              // set original state => draggled item return the original place
              setActionObj(oriActionObj);
              fetchDataErrHandler(error, "change to do list");
            });
        }
        break;
      default:
        return;
    }
  };

  const getRecommend = () => {
    console.log("recommend.");
    getData("todo/readyto/recommend", session.accessToken)
      .then((result) => {
        const recommendList = deepCopy(result);
        recommendList.forEach((action) => {
          const diffDay = moment().diff(moment(action["end_date"]), "day");
          // console.log("diffDay", diffDay);
          action.day = diffDay;
        });
        const newActionObj = deepCopy(actionObj);
        newActionObj.readyto.items = recommendList;
        setActionObj(newActionObj);
        shouldDoMutate();
      })
      .catch((error) => {
        console.log(error);
        fetchDataErrHandler(error, "get recommed actions");
      });
  };

  // 整理 project list
  // 1. filter progress < 1
  // 2. Put 對應的 should do list 進去
  // console.log("project data", projectData);
  // console.log("shouldDo", shouldDoData);
  const showProjectList = projectData.filter((project) => project.progress < 1);
  showProjectList.forEach((project) => {
    const actionList = shouldDoData.filter((action) => {
      return action.root.id === project.id;
    });
    project.actionList = actionList;
  });
  // console.log("project data", showProjectList);
  // console.log("readytoData", actionObj.readyto.items);
  // console.log("doneData", actionObj.done.items);
  // console.log("note list", noteList);
  // console.log("actions", actionObj);

  return (
    <main className="flex h-full min-h-screen grow items-start justify-center overflow-hidden xl:gap-7 xl:px-6 xl:py-8 3xl:gap-9 3xl:px-[2.438rem] 3xl:py-11">
      <DragDropContext onDragEnd={dragEndHandler}>
        {/* should do zone */}
        <div className="h-full w-3/12 flex-auto overflow-y-auto pb-2 pr-3">
          {showProjectList.length > 0 ? (
            <Accordion initOpenId={showProjectList[0].id}>
              {showProjectList.map((project) => {
                return (
                  <div
                    key={project.id}
                    className="w-full cursor-pointer rounded-2xl bg-white shadow dark:bg-black"
                  >
                    <ProjectCard
                      project={project}
                      actionList={project.actionList}
                      showFlag={true}
                      operations={null}
                    />
                  </div>
                );
              })}
            </Accordion>
          ) : (
            <div className="w-full rounded-2xl bg-white text-center text-clamp24 font-semibold shadow dark:bg-black xl:py-10 2xl:py-14">
              目前無專案
            </div>
          )}
        </div>
        {/* to do list zone */}
        <div
          className={`flex h-full w-[calc(100%*0.37)] flex-col gap-4 rounded-2xl bg-white py-3.5 shadow dark:bg-black xl:px-6 3xl:px-9`}
        >
          <div className="text-center text-clamp36 font-medium">
            {toDoListT("title")}
          </div>
          <Droppable droppableId="readyto">
            {(provided, snapshot) => (
              <div
                className={`flex grow flex-col overflow-y-auto pr-1 ${
                  actionObj.readyto.items.length === 0
                    ? "justify-center"
                    : "justify-start"
                }`}
                ref={provided.innerRef}
                {...provided.droppableProps}
              >
                {actionObj.readyto.items.map((action, index) => {
                  return (
                    <Draggable
                      draggableId={action.id.toString()}
                      index={index}
                      key={action.id}
                    >
                      {(provided, snapshot) => (
                        <div
                          className={`action-card xl:mb-2 2xl:mb-2.5 3xl:mb-4 ${
                            snapshot.isDragging ? "bg-white-op50" : ""
                          }`}
                          ref={provided.innerRef}
                          snapshot={snapshot}
                          {...provided.draggableProps}
                          {...provided.dragHandleProps}
                        >
                          <ActionCard
                            itemData={action}
                            nameStyle="text-clamp24 font-semibold"
                            checkHandler={checkHandler}
                          >
                            <div className="flex w-3/12 flex-col items-end justify-center">
                              <div
                                className="w-full truncate text-end text-clamp16 font-bold text-grey"
                                title={action.root.name}
                              >
                                {action.root.name}
                              </div>
                              <div
                                className={`whitespace-nowrap text-clamp18 ${
                                  action.day <= 0 ? "" : "text-danger"
                                }`}
                              >
                                {actionCardT("days", {
                                  dayDiff:
                                    action.day <= 0 ? "moreThan" : "cross",
                                  day: Math.abs(action.day),
                                })}
                              </div>
                            </div>
                          </ActionCard>
                        </div>
                      )}
                    </Draggable>
                  );
                })}
                {shouldDoData.length === 0 &&
                  actionObj.readyto.items.length === 0 && (
                    <div className="text-center text-clamp24 text-grey">
                      {toDoListT("descrip.empty")}
                    </div>
                  )}
                {/* empty UI (do not have any action) */}
                {shouldDoData.length > 0 &&
                  actionObj.readyto.items.length === 0 &&
                  actionObj.done.items.length === 0 && (
                    <div className="flex h-full flex-col items-center justify-center">
                      <div className="mb-28 text-center text-clamp24 leading-relaxed text-grey">
                        {toDoListT.rich("descrip.manual", {
                          p: (chunks) => <p>{chunks}</p>,
                        })}
                      </div>
                      <div>
                        <button
                          className="btn-border-small"
                          onClick={getRecommend}
                        >
                          {acBtnT("recommend")}
                        </button>
                      </div>
                    </div>
                  )}
                {/* recomment UI*/}
                {shouldDoData.length > 0 &&
                  actionObj.readyto.items.length === 0 &&
                  actionObj.done.items.length > 0 && (
                    <div className="flex flex-col items-center justify-center gap-5">
                      <div className="text-center text-clamp24 leading-10 text-grey">
                        {toDoListT.rich("descrip.done", {
                          p: (chunks) => <p>{chunks}</p>,
                        })}
                      </div>
                      <div>
                        <button
                          className="btn-border-small"
                          onClick={getRecommend}
                        >
                          {acBtnT("continue-recommend")}
                        </button>
                      </div>
                    </div>
                  )}
                {provided.placeholder}
              </div>
            )}
          </Droppable>
          <div
            className={`${
              actionObj.readyto.items.length === 0 ? "hidden" : ""
            }`}
          >
            <Droppable droppableId="shoulddo">
              {(provided, snapshot) => (
                <div
                  className={`relative w-fit rounded-full border-4 border-primary xl:p-2.5 3xl:p-3.5 ${
                    snapshot.isDraggingOver ? "bg-primary" : ""
                  }`}
                  ref={provided.innerRef}
                  {...provided.droppableProps}
                >
                  <LuTrash2
                    className={`left-1/2 top-1/2 text-clamp36 transition-all ${
                      snapshot.isDraggingOver
                        ? "absolute -translate-x-1/2 -translate-y-1/2 stroke-white"
                        : ""
                    }`}
                  />
                  {provided.placeholder}
                </div>
              )}
            </Droppable>
          </div>
        </div>
        {/* done zone */}
        <div className="h-3/4 w-3/12 flex-auto overflow-hidden rounded-2xl bg-white pb-6 shadow dark:bg-black">
          <div className="h-3/4 overflow-hidden border-b border-grey pt-6">
            <div className="done-title">
              <div>{itemListT("title.done")}</div>
              <div>
                {formatDate(new Date(), curLang === "ch" ? "zh-CN" : "en-US")}
              </div>
            </div>
            <Droppable droppableId="done">
              {(provided, snapshot) => (
                <div
                  className="flex h-4/5 flex-col overflow-auto px-6 pb-2"
                  ref={provided.innerRef}
                  {...provided.droppableProps}
                >
                  {actionObj.done.items.map((action, index) => {
                    return (
                      <Draggable
                        key={action.id}
                        index={index}
                        draggableId={action.id.toString()}
                      >
                        {(provided, snapshot) => (
                          <div
                            className="mb-3 rounded-2xl bg-white dark:bg-black"
                            ref={provided.innerRef}
                            snapshot={snapshot}
                            {...provided.draggableProps}
                            {...provided.dragHandleProps}
                          >
                            <ActionCard
                              itemData={action}
                              nameStyle={"text-clamp20"}
                            >
                              <button
                                onClick={() => {
                                  editDoneHandler(action.id);
                                }}
                                className="w-fit"
                              >
                                <AiOutlineEdit className="me-2 stroke-black text-clamp24 dark:stroke-white" />
                              </button>
                            </ActionCard>
                          </div>
                        )}
                      </Draggable>
                    );
                  })}
                  {provided.placeholder}
                </div>
              )}
            </Droppable>
          </div>
          <div className="h-1/4 px-6 pt-6">
            <div className="note-title">{itemListT("title.note")}</div>
            {noteList && (
              <div className="relative h-5/6 w-full">
                <ItemList
                  itemList={noteList}
                  bulletColor="bg-danger"
                  editable={true}
                  storeHandler={storeHandler}
                  showSaveBtn={showSaveBtn}
                  setShowSaveBtn={setShowSaveBtn}
                  listContainerRef={listContainerRef}
                />
              </div>
            )}
          </div>
        </div>
      </DragDropContext>
    </main>
  );
}

export default UserAuthHOC(ToDoListPage);

// const dummyData2 = [
//   { id: 1, text: "官網首頁 RWD", note: true },
//   {
//     id: 2,
//     text: "盤點設變盤點設變盤點設變盤點設變盤點設變盤點設變盤點設變盤點設變盤點設變",
//     note: true,
//   },
//   { id: 3, text: "好市多買食材" },
//   { id: 4, text: "好市多買食材" },
//   { id: 5, text: "好市多買食材" },
//   { id: 6, text: "好市多買食材" },
// ];

// const dummyData = [
//   {
//     id: 1,
//     root: { name: "WMS" },
//     name: "入庫設變入庫設變入庫設變入庫設變入庫設變入庫設變入庫設變",
//     day: 300,
//   },
//   {
//     id: 2,
//     root: { name: "WMS" },
//     name: "入庫設變入庫設變入庫設變入庫設變入庫設變入庫設變入庫設變",
//     day: -2,
//   },
//   {
//     id: 3,
//     root: { name: "WMS" },
//     name: "入庫設變入庫設變入庫設變入庫設變入庫設變入庫設變入庫設變",
//     day: 0,
//   },
//   {
//     id: 4,
//     root: { name: "WMS" },
//     name: "入庫設變入庫設變入庫設變入庫設變入庫設變入庫設變入庫設變",
//     day: 300,
//   },
//   {
//     id: 5,
//     root: { name: "WMS" },
//     name: "入庫設變入庫設變入庫設變入庫設變入庫設變入庫設變入庫設變",
//     day: 300,
//   },
//   {
//     id: 6,
//     root: { name: "WMS" },
//     name: "入庫設變入庫設變入庫設變入庫設變入庫設變入庫設變入庫設變",
//     day: 300,
//   },
//   {
//     id: 7,
//     root: { name: "WMS" },
//     name: "入庫設變入庫設變入庫設變入庫設變入庫設變入庫設變入庫設變",
//     day: 300,
//   },
//   {
//     id: 8,
//     root: { name: "WMS" },
//     name: "入庫設變入庫設變入庫設變入庫設變入庫設變入庫設變入庫設變",
//     day: 300,
//   },
//   {
//     id: 9,
//     root: { name: "WMS" },
//     name: "入庫設變入庫設變入庫設變入庫設變入庫設變入庫設變入庫設變",
//     day: 300,
//   },
//   {
//     id: 10,
//     root: { name: "WMS" },
//     name: "入庫設變入庫設變入庫設變入庫設變入庫設變入庫設變入庫設變",
//     day: 300,
//   },
//   {
//     id: 11,
//     root: { name: "WMS" },
//     name: "入庫設變入庫設變入庫設變入庫設變入庫設變入庫設變入庫設變",
//     day: 300,
//   },
// ];
