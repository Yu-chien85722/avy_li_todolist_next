import SignInS from "@/app/_component/SignInS";
import SignInC from "@/app/_component/SignInC";

export default async function signInPage({ searchParams }) {
  return (
    <>
      <div className="flex min-h-full bg-white dark:bg-black">
        <div className="flex w-8/12 items-center justify-center">
          <div className="-translate-x-6 -translate-y-6 font-medium">
            <span className="text-clamp48">讓 Ivy 幫你安排專案進度</span>
            <br />
            <span className="text-clamp36">
              You&#8242;ll know what to do every day at a glance👀
            </span>
          </div>
        </div>
        <div className="from-40.63% flex w-4/12 items-center justify-center bg-gradient-147 from-danger-op40 via-primary-op40 to-success-op40">
          <div className="">
            {/* <SignInC searchParams={searchParams} /> */}
            <SignInS searchParams={searchParams} />
          </div>
        </div>
      </div>
    </>
  );
}
