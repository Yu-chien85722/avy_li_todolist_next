"use client";

import Image from "next/legacy/image";
import notFoundImg from "./images/404.png";

export default function GlobalNotFound() {
  return (
    <html lang="en">
      <body className="flex items-center justify-center">
        <Image src={notFoundImg} alt="404 Error" priority />
      </body>
    </html>
  );
}
