import React from "react";
import cx from "classnames";

const ProgressBar = ({ progress, color }) => {
  return (
    <div className="w-full rounded-full border border-grey xl:h-3 3xl:h-4.5">
      <div
        className={cx(`h-full bg-${color} rounded-l-full`, {
          [`rounded-r-full`]: progress > 96,
        })}
        style={{ width: `${progress}%` }}
      ></div>
    </div>
  );
};

export default React.memo(ProgressBar);
