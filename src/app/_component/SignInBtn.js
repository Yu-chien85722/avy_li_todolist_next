"use client";

import React from "react";
import { signIn } from "next-auth/react";
import Button from "./Button";
import { FcGoogle } from "react-icons/fc";

const SignInBtn = ({ provider }) => {
  return (
    <Button
      title={{
        className:
          "rounded-lg border border-1 border-black pl-5 pr-16 py-1 text-clamp14",
        text: `Sign in/up with ${provider.name}`,
      }}
      clickHandler={() => signIn(provider.id)}
    >
      <FcGoogle className="h-8 w-8" />
    </Button>
  );
};

export default React.memo(SignInBtn);
