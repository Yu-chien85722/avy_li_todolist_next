"use client";
import React, { useEffect, useState, useCallback } from "react";
import { Calendar, momentLocalizer } from "react-big-calendar";
import moment from "moment/moment";
import "../style/calendar.scss";
import cx from "classnames";
import {
  BsChevronLeft,
  BsChevronRight,
  BsCheckLg,
  BsFillCircleFill,
} from "react-icons/bs";
import { getData } from "../api";
import { fetchDataErrHandler } from "../lib/global";
import UserAuthHOC from "./HOC/UserAuthHOC";

const localizer = momentLocalizer(moment);

const MyCalendar = ({
  mode,
  selectDate,
  setSelectDate,
  memoSaveTime,
  session,
  status,
}) => {
  // console.log("outer", selectDate);

  const today = moment().startOf("day");
  const [events, setEvents] = useState([]);
  const [memos, setMemos] = useState([]);
  const [checks, setChecks] = useState([]);
  const [currentMonth, setCurrentMonth] = useState(moment().format("YYYY-MM"));

  const handleSelectSlot = (slotInfo) => {
    // alert(slotInfo.start);
    // alert(moment(slotInfo.start).format("YYYY-MM-DD"));
    setSelectDate(moment(slotInfo.start).format("YYYY-MM-DD"));
    // console.log(slotInfo);
  };

  const CustomDateCellWrp = ({ value }) => {
    // value = render date
    // console.log(value);
    // console.log(selectDate);
    const isSelected = selectDate
      ? moment(value).startOf("day").isSame(selectDate)
      : false;
    // const isOffDay = moment(value).
    return (
      <div
        className={`rbc-day-bg ${isSelected ? "selected" : ""} ${
          false ? "rbc-off-range-bg" : ""
        }`}
      ></div>
    );
  };

  const CustomDateCell = ({ date, label }) => {
    // console.log(date);
    // console.log(today);
    const isToday = moment(date).startOf("day").isSame(today);
    const isHoliday = events?.some((event) =>
      moment(event.date).isSame(date, "day"),
    );
    const hasMemo = memos?.some((memo) => moment(memo).isSame(date, "day"));
    const hasCheck = checks?.some((check) => moment(check).isSame(date, "day"));
    return (
      <div
        className={`rbc-custom-date-cell ${
          mode === "calendar" ? "cursor-pointer" : ""
        }`}
      >
        <div
          className={cx(
            "text-center",
            { [`w-[2.575rem]`]: mode === "calendar" },
            { [`w-[1.45rem]`]: mode === "dsCalendar" },
            { today: isToday },
            { holiday: isHoliday },
          )}
        >
          {label.charAt(0) === "0" ? label.slice(1) : label}
        </div>
        {hasCheck && <BsCheckLg className="fill-success text-clamp28" />}
        {hasMemo && <BsFillCircleFill className="fill-primary text-clamp14" />}
      </div>
    );
  };

  const CustomToolbar = ({ label, onNavigate }) => {
    return (
      <div className="flex justify-between xl:mb-3 2xl:mb-4 3xl:mb-6">
        <span
          className={`text-clamp24 font-medium ${
            mode === "calendar" ? "hidden" : ""
          }`}
        >
          {label}
        </span>
        <div className="flex gap-8">
          <button className="toolbar-button" onClick={() => onNavigate("PREV")}>
            <BsChevronLeft
              className={`fill-primary ${
                mode === "calendar" ? "text-clamp36" : "text-clamp20"
              }`}
            />
          </button>
          <span
            className={`text-clamp28 font-medium ${
              mode === "calendar" ? "" : "hidden"
            }`}
          >
            {label}
          </span>
          <button className="toolbar-button" onClick={() => onNavigate("NEXT")}>
            <BsChevronRight
              className={`fill-primary ${
                mode === "calendar" ? "text-clamp36" : "text-clamp20"
              }`}
            />
          </button>
        </div>
      </div>
    );
  };

  // when current date changed, this callback func will be executed
  // if current date is 7/4(new Date() obj), after user click next month, the current date will changed to 8/4 (new Date() obj) => execute func
  // newDate = current date
  const handleNavigate = useCallback(
    (newDate) => {
      // string
      const newMonth = moment(newDate).format("YYYY-MM");
      // update current month
      if (newMonth !== currentMonth) {
        setCurrentMonth(newMonth);
      }
    },
    [currentMonth],
  );

  const getCheckMemoList = useCallback(() => {
    setChecks([]);
    setMemos([]);
    // console.log("get check memo event.");
    const currentYear = currentMonth.split("-")[0];
    const curMonth = currentMonth.split("-")[1];
    // console.log(currentYear, curMonth);
    getData(
      `calendar/thing/simple/${currentYear}/${curMonth}`,
      session.accessToken,
    )
      .then((result) => {
        // console.log(result);
        let checkArr = [];
        let memoArr = [];
        for (let date in result) {
          const hasChecked = result[date].actionCheck;
          const hasMemo = result[date].memoCheck;
          if (hasChecked) {
            checkArr.push(date);
          }
          if (hasMemo) {
            memoArr.push(date);
          }
        }
        setChecks(checkArr);
        setMemos(memoArr);
        // console.log("check", checkArr);
        // console.log("memo", memoArr);
      })
      .catch((err) => {
        // console.log(err);
        fetchDataErrHandler(err, "get mark list");
      });
  }, [currentMonth, setChecks, setMemos, session?.accessToken]);

  const getEventList = useCallback(() => {
    setEvents([]);
    // console.log("get event.");
    const currentYear = currentMonth.split("-")[0];
    const curMonth = currentMonth.split("-")[1];
    // console.log(currentYear, curMonth);
    getData(`calendar/${currentYear}/${curMonth}`, session.accessToken)
      .then((result) => {
        // console.log(result);
        setEvents(result);
      })
      .catch((err) => {
        fetchDataErrHandler(err, "get holiday");
      });
  }, [currentMonth, session?.accessToken]);

  useEffect(() => {
    if (status === "authenticated") {
      if (mode === "calendar") {
        getCheckMemoList();
      }
      getEventList();
    }
  }, [
    currentMonth,
    mode,
    status,
    memoSaveTime,
    getCheckMemoList,
    getEventList,
  ]);

  return (
    <Calendar
      localizer={localizer}
      events={events}
      startAccessor="start"
      endAccessor="end"
      views={["month"]}
      selectable="ignoreEvents"
      // eventPropGetter={mode === "calendar" ? eventStyleGetter : false}
      onSelectSlot={mode === "calendar" ? handleSelectSlot : false}
      onNavigate={handleNavigate}
      components={{
        toolbar: CustomToolbar,
        month: {
          dateHeader: CustomDateCell, // Use custom date cell component
          dateCellWrapper: CustomDateCellWrp,
        },
      }}
    />
  );
};

export default UserAuthHOC(MyCalendar);

// dummy data
// 產生指定範圍的隨機數字
const generateRandomNumber = (min, max) => {
  const randomNumber = Math.floor(Math.random() * (max - min + 1)) + min;
  return randomNumber.toString();
};

// 取得隨機的日期
const generateRandomDate = (specMonth) => {
  const year = generateRandomNumber(2023, 2024);
  const month = generateRandomNumber(specMonth, specMonth);
  const day = generateRandomNumber(1, 15);
  // const hours = generateRandomNumber(0, 23);
  // const minutes = generateRandomNumber(0, 59);
  // const seconds = generateRandomNumber(0, 59);

  // return `${year}-${month.toString().padStart(2, '0')}-${day.toString().padStart(2, '0')} ${hours.toString().padStart(2, '0')}:${minutes
  // 	.toString()
  // 	.padStart(2, '0')}:${seconds.toString().padStart(2, '0')}`;
  return `${year}-${month.toString().padStart(2, "0")}-${day
    .toString()
    .padStart(2, "0")}`;
};

const generateRadomMark = (month) => {
  const marks = [];
  for (let i = 0; i < 3; i++) {
    const mark = {
      date: generateRandomDate(month),
      memo: "memo",
    };
    marks.push(mark);
  }
  return marks;
};
