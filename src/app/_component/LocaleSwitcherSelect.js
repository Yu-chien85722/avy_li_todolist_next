"use client";

import cls from "classnames";
import { useParams } from "next/navigation";
import { useTransition } from "react";
import { useRouter, usePathname } from "@/navigation";
import tailwindConfig from "../../../tailwind.config";

export default function LocaleSwitcherSelect({
  children,
  defaultValue,
  label,
}) {
  const router = useRouter();
  const [isPending, startTransition] = useTransition();
  const pathname = usePathname();
  const params = useParams();

  // console.log("pathname", pathname);
  // console.log("params", params);

  function onSelectChange(event) {
    const nextLocale = event.target.value;
    startTransition(() => {
      router.replace(
        // @ts-expect-error -- TypeScript will validate that only known `params`
        // are used in combination with a given `pathname`. Since the two will
        // always match for the current route, we can skip runtime checks.
        { pathname, params },
        { locale: nextLocale },
      );
    });
  }

  return (
    <label
      className={cls(
        "relative text-grey",
        isPending && "transition-opacity [&:disabled]:opacity-30",
      )}
    >
      <p className="sr-only">{label}</p>
      <select
        className="inline-flex cursor-pointer appearance-none rounded-2xl border border-primary bg-transparent py-3 pl-2 pr-10 focus:outline-none focus-visible:outline-none"
        defaultValue={defaultValue}
        disabled={isPending}
        onChange={onSelectChange}
      >
        {children}
      </select>
      <span className="pointer-events-none absolute right-2 top-[0.5rem]">
        <svg
          xmlns="http://www.w3.org/2000/svg"
          width="14"
          height="11"
          viewBox="0 0 14 11"
          fill="none"
          className="absolute right-3 top-3"
        >
          <path
            d="M7.83467 9.73501C7.43943 10.334 6.56057 10.334 6.16533 9.73501L1.09502 2.05074C0.65633 1.38589 1.13315 0.5 1.9297 0.5L12.0703 0.5C12.8669 0.5 13.3437 1.38589 12.905 2.05074L7.83467 9.73501Z"
            fill={tailwindConfig.theme.extend.colors.primary.DEFAULT}
          />
        </svg>
      </span>
    </label>
  );
}
