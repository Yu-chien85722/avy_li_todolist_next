"use client";

import React, { useRef, useContext, useMemo } from "react";
import { accrodionContext } from "./Accordion";
import ProgressBar from "./ProgressBar";
import { FiFlag } from "react-icons/fi";
import { useTranslations } from "next-intl";
import EllipseDropdown from "./EllipseDropdown";
import ActionCard from "./ActionCard";
import moment from "moment";
import { Draggable, Droppable } from "react-beautiful-dnd";

const ProjectCard = ({ project, actionList, showFlag = false, operations }) => {
  const { openId, setOpenId } = useContext(accrodionContext) ?? {
    openId: undefined,
    setOpenId: undefined,
  };
  const t = useTranslations("ProjectCard");
  const actionCardT = useTranslations("ActionCard");
  const { id, name, priority, curItem, startDate, endDate, progress } =
    useMemo(() => {
      const startDate = project.begin_date.slice(0, 10).replace(/-/g, "/");
      const endDate = project.end_date.slice(0, 10).replace(/-/g, "/");
      const curItem = project.next_child?.name;
      const progress = parseInt(Number(project.progress) * 100);
      return {
        ...project,
        startDate,
        endDate,
        curItem,
        progress,
      };
    }, [project]);
  // console.log("project", project);

  let flagColor = "";
  switch (priority) {
    case 2:
      flagColor = "danger";
      break;
    case 1:
      flagColor = "primary";
      break;
    default:
      flagColor = "success";
  }

  return (
    <>
      <div
        className="relative flex flex-col xl:p-3 3xl:p-6"
        onClick={() => {
          if (setOpenId) {
            if (openId === id) {
              setOpenId(null);
            } else {
              setOpenId(id);
            }
          }
        }}
      >
        {operations && (
          <EllipseDropdown>
            {/* dropdown menu */}
            <div
              id="operate-dropdownMenu"
              className="absolute right-4 top-8 w-36 rounded-lg border border-grey-163 bg-white dark:bg-black"
            >
              <div
                data-id={id}
                className="rounded-t-lg py-2 pl-4 text-sm hover:bg-primary-100 dark:hover:text-black"
                onClick={(e) => {
                  operations.onEditProject(e);
                }}
              >
                {t("editProject")}
              </div>
              <div
                className="rounded-b-lg py-2 pl-4 text-sm hover:bg-primary-100 dark:hover:text-black"
                data-id={id}
                onClick={(e) => {
                  operations.onDelProject(e);
                }}
              >
                {t("delProject")}
              </div>
            </div>
          </EllipseDropdown>
        )}
        <div className="flex items-center xl:mb-3.5 3xl:mb-4">
          <div className="flex w-1/2 items-center">
            <div
              className="me-3 truncate text-clamp24 font-semibold"
              title={name}
            >
              {name}
            </div>
            {showFlag && (
              <div className="me-3">
                <FiFlag
                  className={`stroke-black text-clamp20 fill-${flagColor} dark:stroke-white`}
                />
              </div>
            )}
          </div>
          <div
            className={`flex ${operations ? "w-2/5" : "w-1/2"} text-clamp18`}
          >
            {progress < 100 && (
              <>
                <span className="whitespace-nowrap">{t("current")}：</span>
                <span className="truncate" title={curItem}>
                  {curItem}{" "}
                </span>
              </>
            )}
          </div>
        </div>
        <div className="flex items-center">
          <div className="w-1/2 text-grey">
            <div className="whitespace-nowrap text-clamp16">
              {t("startDate")}：{startDate}
            </div>
            <div className="whitespace-nowrap text-clamp16">
              {t("endDate")}：{endDate}
            </div>
          </div>
          <div className="flex w-1/2 items-center">
            <div className="me-4 grow">
              <ProgressBar color="primary" progress={progress} />
            </div>
            <div className="text-clamp18">{progress}%</div>
          </div>
        </div>
      </div>
      {openId === id && (
        <Droppable droppableId={`shoulddo-${id}`}>
          {(provided, snapshot) => (
            <div
              className="overflow-y-atuo flex flex-col rounded-b-2xl border-t border-grey bg-white shadow dark:bg-black xl:p-3 3xl:p-6"
              ref={provided.innerRef}
              {...provided.droppableProps}
            >
              {actionList?.map((action, index) => {
                const diffDay = moment().diff(
                  moment(action["end_date"]),
                  "day",
                );
                // console.log("diffDay", diffDay);
                action.day = diffDay;
                return (
                  <Draggable
                    key={action.id}
                    index={index}
                    draggableId={action.id.toString()}
                  >
                    {(provided, snapshot) => (
                      <div
                        className="rounded-2xl bg-white dark:bg-black xl:mb-3 3xl:mb-4"
                        ref={provided.innerRef}
                        snapshot={snapshot}
                        {...provided.draggableProps}
                        {...provided.dragHandleProps}
                      >
                        <ActionCard itemData={action} nameStyle="text-clamp20">
                          <div className="flex w-3/12 flex-col items-end justify-center">
                            <div
                              className={`whitespace-nowrap text-sm ${
                                action.day <= 0 ? "" : "text-danger"
                              }`}
                            >
                              {actionCardT("days", {
                                dayDiff: action.day <= 0 ? "moreThan" : "cross",
                                day: Math.abs(action.day),
                              })}
                            </div>
                          </div>
                        </ActionCard>
                      </div>
                    )}
                  </Draggable>
                );
              })}
              {provided.placeholder}
            </div>
          )}
        </Droppable>
      )}
    </>
  );
};

export default React.memo(ProjectCard);
