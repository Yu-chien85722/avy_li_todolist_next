import React from "react";
import { Link } from "@/navigation";
import { useLocale } from "next-intl";

const LinkCard = ({ icon, name, route, active }) => {
  const curLang = useLocale();
  return (
    <Link
      className={`flex items-center justify-start rounded-l-2xl py-2.5 pl-2.5 pr-3 xl:gap-3 3xl:gap-4 ${
        active
          ? "from-2.37% bg-gradient-95 from-primary-op20 to-success-op20"
          : ""
      }`}
      href={route}
    >
      {icon}
      <div
        className={`text-clamp24 font-semibold leading-relaxed ${
          curLang === "ch" ? "tracking-[.36em]" : ""
        }`}
      >
        {name}
      </div>
    </Link>
  );
};
export default React.memo(LinkCard);
