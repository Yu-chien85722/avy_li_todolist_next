"use client";

import React from "react";
import {
  // Gantt,
  ViewMode,
} from "gantt-task-react";
import ViewSwitcher from "./ViewSwitcher";
import { Gantt } from "../lib/cusGantt";
import "../style/myGantt.css";
import tailwindConfig from "../../../tailwind.config";
import { useEffect, useState, useRef } from "react";
import { getData, putData } from "../api";
import useSWR from "swr";
import { fetchDataErrHandler } from "../lib/global";
import moment from "moment";
import Loading from "@/app/[locale]/loading";
import FailLoad from "@/app/_component/FailLoad";
import { useTranslations } from "next-intl";
import UserAuthHOC from "@/app/_component/HOC/UserAuthHOC";

const currentDate = new Date();

let taskList = [
  {
    start: new Date(2000, 1, 1),
    end: new Date(2000, 1, 15),
    name: "Some Project",
    id: "ProjectSample",
    progress: 25,
    type: "project",
    hideChildren: false,
  },
  {
    start: new Date(currentDate.getFullYear(), currentDate.getMonth(), 1),
    end: new Date(currentDate.getFullYear(), currentDate.getMonth(), 2, 12, 28),
    name: "Idea",
    id: "Task 0",
    progress: 45,
    type: "task",
    project: "ProjectSample",
  },
  // {
  //   start: new Date(currentDate.getFullYear(), currentDate.getMonth(), 2),
  //   end: new Date(currentDate.getFullYear(), currentDate.getMonth(), 4, 0, 0),
  //   name: "Research",
  //   id: "Task 1",
  //   progress: 25,
  //   dependencies: ["Task 0"],
  //   type: "task",
  //   project: "ProjectSample",
  // },
];

const toGanttData = (data, type, displayOption) => {
  const ganttDataObj = {};
  ganttDataObj.id = type + "-" + data.id;
  ganttDataObj.name = data.name;
  ganttDataObj.type = type;
  ganttDataObj.start = new Date(data["begin_date"]);
  ganttDataObj.end = new Date(data["end_date"]);
  ganttDataObj.progress = Number(data.progress) * 100;
  ganttDataObj.styles = {
    backgroundBorderColor: tailwindConfig.theme.extend.colors.primary.DEFAULT,
    backgroundColor: "rgba(255,255,255,0.5)",
    backgroundSelectedColor: tailwindConfig.theme.extend.colors.primary[100],
    progressColor: tailwindConfig.theme.extend.colors.primary.DEFAULT,
    progressSelectedColor: tailwindConfig.theme.extend.colors.primary.DEFAULT,
  };
  ganttDataObj.isDisabled = displayOption.isDisabled;
  if (type === "project") {
    ganttDataObj.hideChildren = displayOption.isHideChildren;
  }

  if (type === "task") {
    ganttDataObj.project = "project-" + data.root;
  }
  // console.log("ganttDataObj", ganttDataObj);
  return ganttDataObj;
};

function getStartEndDateForProject(tasks, projectId) {
  const projectTasks = tasks.filter((t) => t.project === projectId);
  let start = projectTasks[0].start;
  let end = projectTasks[0].end;

  for (let i = 0; i < projectTasks.length; i++) {
    const task = projectTasks[i];
    if (start.getTime() > task.start.getTime()) {
      start = task.start;
    }
    if (end.getTime() < task.end.getTime()) {
      end = task.end;
    }
  }
  return [start, end];
}

const MyGantt = ({
  session,
  status,
  showDone,
  showViewSwitcher = false,
  isDisabled,
  isHideChildren,
  displayOption,
  getGanttAPI,
  missionMutate,
  projectData,
}) => {
  const ganttParentRef = useRef();
  const [ganttParentHeight, setGanttParentHeight] = useState(0);
  const [ganttHeight, setGanttHeight] = useState(0);
  const [tasks, setTasks] = useState([]);
  const [view, setView] = useState(ViewMode.Month);
  const [viewDate, setViewDate] = useState(new Date());
  const t = useTranslations("MyGantt");

  // get project data
  const {
    data: projects,
    error,
    isLoading,
    mutate,
  } = useSWR(
    session ? [getGanttAPI, session.accessToken] : null,
    ([url, token]) => getData(url, token),
  );

  useEffect(() => {
    if (!isLoading && projects && projects.length > 0) {
      setTasks(makeGanttTasks());
    } else {
      setTasks([]);
    }
  }, [isLoading, projects]);

  // if mission data changed, reload gantt chart
  useEffect(() => {
    // console.log("mission change.");
    mutate();
  }, [projectData, mutate]);

  useEffect(() => {
    // console.log("ganttParentHeight", ganttParentHeight);
    let viewSwitchHeight = 0;
    if (showViewSwitcher) {
      viewSwitchHeight = 46;
    }
    setGanttHeight(ganttParentHeight - viewSwitchHeight - 78);
  }, [ganttParentHeight, showViewSwitcher]);

  useEffect(() => {
    if (!isLoading && tasks.length > 0) {
      setGanttParentHeight(ganttParentRef.current?.offsetHeight);
    }
  }, [isLoading, tasks]);

  useEffect(() => {
    const resizeEvent = window.addEventListener("resize", () => {
      // console.log("resize");
      if (!isLoading && tasks.length > 0) {
        setGanttParentHeight(ganttParentRef.current?.offsetHeight);
      }
    });
    return () => {
      window.removeEventListener("resize", resizeEvent);
    };
  }, [isLoading, tasks]);

  if (status === "loading" || isLoading) {
    return <Loading />;
  }

  if (error) {
    fetchDataErrHandler(error, "load Gantt");
    return <FailLoad />;
  }

  if (!isLoading && status !== "loading" && tasks.length === 0) {
    return (
      <div className="absolute left-1/2 top-1/2 -translate-x-1/2 -translate-y-1/2 text-clamp24 text-grey">
        {t("emptyDescrip")}
      </div>
    );
  }

  function makeGanttTasks() {
    // console.log("make gantt tasks", projects);
    let finalProjects = projects;
    let projectList = [];
    // 依據參數跟 projects data 整理成 Gantt 能吃的格式
    if (!showDone) {
      finalProjects = projects.filter(
        (project) => Number(project.progress) < 1,
      );
    }
    // console.log(finalProjects);
    finalProjects.forEach((project) => {
      const projectGanttDataObj = toGanttData(project, "project", {
        isDisabled,
        isHideChildren,
      });
      projectList.push(projectGanttDataObj);
      project.children?.forEach((mission) => {
        const missionGanttDataObj = toGanttData(mission, "task", {
          isDisabled,
          isHideChildren,
        });
        projectList.push(missionGanttDataObj);
      });
    });
    return projectList;
  }

  // const handleProgressChange = async (task) => {
  //   // setTasks(tasks.map((t) => (t.id === task.id ? task : t)));
  //   console.log("On progress change Id:", task);
  // };

  // when expand project or close project
  const handleExpanderClick = (task) => {
    console.log(task);
    setViewDate(null);
    setTasks(tasks.map((t) => (t.id === task.id ? task : t)));
    console.log("On expander click Id:" + task.id);
  };

  // when task change begin date and end date
  const handleTaskChange = (task) => {
    setViewDate(null);

    // 更動日期的那個 task
    console.log("On date change Id:", task);
    // update mission beging date and end date to backend
    const updateMData = {
      name: task.name,
      begin_date: moment(task.start).format("YYYY-MM-DD"),
      end_date: moment(task.end).format("YYYY-MM-DD"),
    };
    const taskId = task.id.slice(5);
    console.log(taskId);
    putData(
      `project/mission/${taskId}`,
      session.accessToken,
      JSON.stringify(updateMData),
    )
      .then((result) => {
        if (result) {
          let newTasks = tasks.map((t) => (t.id === task.id ? task : t));
          // 將更新的 task 更新到 tasks
          setTasks(newTasks);
          // reload mission data to re-render mission cards
          missionMutate();
        }
      })
      .catch((error) => {
        fetchDataErrHandler(error, "update mission");
        // 用舊的 gantt data 重 render 一次
        setTasks(makeGanttTasks());
      });

    // 如該 task 有父親 project
    // if (task.project) {
    //   // 依據更新的 task 的起訖決定 project 的開始與結束時間
    //   const [start, end] = getStartEndDateForProject(newTasks, task.project);
    //   const project =
    //     newTasks[newTasks.findIndex((t) => t.id === task.project)];
    //   if (
    //     project.start.getTime() !== start.getTime() ||
    //     project.end.getTime() !== end.getTime()
    //   ) {
    //     const changedProject = { ...project, start, end };
    //     newTasks = newTasks.map((t) =>
    //       t.id === task.project ? changedProject : t,
    //     );
    //   }
    // }
  };

  // 日期真正的格線在左側
  return (
    // gantt-container flex h-full flex-col items-start justify-start
    <div className="h-full rounded-2xl" ref={ganttParentRef}>
      {
        // week and month
        showViewSwitcher && (
          <div className="mb-3 flex items-center justify-end">
            <ViewSwitcher onViewModeChange={setView} view={view}></ViewSwitcher>
          </div>
        )
      }
      <Gantt
        ganttHeight={ganttHeight}
        // tasks={[]}
        tasks={tasks}
        // default show month
        viewMode={view}
        columnWidth={displayOption.columnWidth}
        viewDate={viewDate}
        barCornerRadius={5}
        listCellWidth={displayOption.listCellWidth}
        // listCellWidth="130px"
        onDateChange={handleTaskChange}
        // set null if can't change progress
        onProgressChange={null}
        onExpanderClick={handleExpanderClick}
      />
    </div>
  );
};

export default React.memo(UserAuthHOC(MyGantt));
