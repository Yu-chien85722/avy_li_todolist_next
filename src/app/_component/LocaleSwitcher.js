"use client";

import cls from "classnames";
import { useLocale, useTranslations } from "next-intl";
import { locales } from "../lib/global";
import LocaleSwitcherDropdown from "./LocaleSwitcherDropdown";

export default function LocaleSwitcher() {
  const t = useTranslations("LocaleSwitcher");
  const locale = useLocale();

  return (
    <LocaleSwitcherDropdown>
      {locales.map((cur) => (
        <div
          key={cur}
          className={cls("flex items-center justify-start px-3 py-2", {
            ["text-primary"]: locale === cur,
          })}
          data-lang={cur}
        >
          {t("locale", { locale: cur })}
        </div>
      ))}
    </LocaleSwitcherDropdown>
  );
}
