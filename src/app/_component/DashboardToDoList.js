"use client";

import useSWR from "swr";
import { getData, putData } from "../api";
import ActionCard from "./ActionCard";

import React from "react";
import { useTranslations } from "next-intl";
import moment from "moment/moment";
import { fetchDataErrHandler } from "../lib/global";
import Loading from "../[locale]/loading";
import FailLoad from "@/app/_component/FailLoad";
import { useRouter } from "@/navigation";
import UserAuthHOC from "@/app/_component/HOC/UserAuthHOC";

function DashboardToDoList({ session, status }) {
  // intel
  const t = useTranslations("ToDoList");
  const actionCardT = useTranslations("ActionCard");
  // get current date
  const today = moment(new Date()).format("YYYY-MM-DD");
  const router = useRouter();
  // 一開始 session is undefined，因為正在 loading 中，loading 完成後
  // session 有值 => re-render => useSWR 就會得到 token 成功取得資料
  const { data, error, isLoading, mutate } = useSWR(
    session ? [`todo/readyto/${today}`, session.accessToken] : null,
    ([url, token]) => getData(url, token),
  );
  // console.log("render todoList");
  const checkHandler = (id) => {
    console.log(id);
    // put to backendar
    putData(
      `todo/done/${id}`,
      session.accessToken,
      JSON.stringify({ status: "Done" }),
    )
      .then((result) => {
        // sucess update to do list
        if (result) {
          mutate();
          router.refresh();
        }
      })
      .catch((error) => {
        fetchDataErrHandler(error, "refresh todoList");
      });
  };

  if (status === "loading" || isLoading) {
    return <Loading />;
  }
  if (error) {
    fetchDataErrHandler(error, "get todoList");
  }
  // 改成前端可以吃的格式
  data?.forEach((action) => {
    const diffDay = moment().diff(moment(action["end_date"]), "day");
    // console.log("diffDay", diffDay);
    action.day = diffDay;
  });

  // console.log(data);
  return (
    <div
      className={`flex h-full w-full flex-col gap-4 rounded-2xl bg-white py-3.5 shadow dark:bg-black xl:px-6 3xl:px-9`}
    >
      <div className="text-center text-clamp36 font-medium">{t("title")}</div>
      {error && <FailLoad />}
      <div className="flex grow flex-col overflow-y-auto pr-1 xl:gap-2 2xl:gap-2.5 3xl:gap-4">
        {!error &&
          (data.length > 0 ? (
            data.map((action) => {
              return (
                <div key={action.id} className="action-card">
                  <ActionCard
                    itemData={action}
                    nameStyle="text-clamp24 font-semibold"
                    checkHandler={checkHandler}
                  >
                    <div className="flex w-3/12 flex-col items-end justify-center">
                      <div
                        className="w-full truncate text-end text-clamp16 font-bold text-grey"
                        title={action.root.name}
                      >
                        {action.root.name}
                      </div>
                      <div
                        className={`whitespace-nowrap text-clamp18 ${
                          action.day <= 0 ? "" : "text-danger"
                        }`}
                      >
                        {actionCardT("days", {
                          dayDiff: action.day <= 0 ? "moreThan" : "cross",
                          day: Math.abs(action.day),
                        })}
                      </div>
                    </div>
                  </ActionCard>
                </div>
              );
            })
          ) : (
            <div className="flex h-full items-center justify-center text-clamp24 text-grey">
              {t("descrip.empty")}
            </div>
          ))}
      </div>
    </div>
  );
}

export default UserAuthHOC(DashboardToDoList);

const dummyData = [
  {
    id: 1,
    projectName: "WMS",
    itemName: "入庫設變入庫設變入庫設變入庫設變入庫設變入庫設變入庫設變",
    day: 300,
  },
  {
    id: 2,
    projectName: "WMS",
    itemName: "入庫設變入庫設變入庫設變入庫設變入庫設變入庫設變入庫設變",
    day: -2,
  },
  {
    id: 3,
    projectName: "WMS",
    itemName: "入庫設變入庫設變入庫設變入庫設變入庫設變入庫設變入庫設變",
    day: 0,
  },
  {
    id: 4,
    projectName: "WMS",
    itemName: "入庫設變入庫設變入庫設變入庫設變入庫設變入庫設變入庫設變",
    day: 300,
  },
  {
    id: 5,
    projectName: "WMS",
    itemName: "入庫設變入庫設變入庫設變入庫設變入庫設變入庫設變入庫設變",
    day: 300,
  },
  {
    id: 6,
    projectName: "WMS",
    itemName: "入庫設變入庫設變入庫設變入庫設變入庫設變入庫設變入庫設變",
    day: 300,
  },
];
