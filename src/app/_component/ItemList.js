"use client";

import React, { useEffect } from "react";
import { useRef, useState } from "react";
import { AiOutlineEye } from "react-icons/ai";
import { BsCheckLg } from "react-icons/bs";
import cx from "classnames";
import { useTranslations } from "next-intl";

// use debounce 自動儲存修改的 memo

const ItemList = ({
  itemList,
  bulletColor,
  editable = true,
  detailCb = () => {},
  storeHandler,
  showSaveBtn,
  setShowSaveBtn,
  listContainerRef,
}) => {
  // Intl
  const t = useTranslations("ItemList");
  const [itemArr, setItemArr] = useState(itemList);
  const [curAction, setCurAction] = useState({});
  const [pickId, setPickId] = useState(0);

  const elementsRef = useRef(itemArr.map(() => React.createRef()));
  // console.log("elements ref", elementsRef.current);
  // console.log("bulletColor", bulletColor);
  // console.log("item list", itemList);
  // console.log("itemArr", itemArr);
  const setCarat = (element) => {
    if (element === undefined) {
      return;
    }
    // console.log(element);
    element.focus();
    window.getSelection().selectAllChildren(element);
    window.getSelection().collapseToEnd();
  };

  const handleItemChange = (e, index) => {
    const newText = e.target.textContent;
    setItemArr((prevState) => {
      const newItemArr = prevState.map((item, i) => {
        if (i === index) {
          return { ...item, text: newText };
        }
        return item;
      });
      return newItemArr;
    });
  };

  const handleKeyDown = (e, index) => {
    if (e.key === "Enter") {
      e.preventDefault();
      setCurAction({ type: "enter", index: index });
      setItemArr((prevState) => [
        ...prevState.slice(0, index + 1),
        { text: "" },
        ...prevState.slice(index + 1),
      ]);
      elementsRef.current.splice(index + 1, 0, React.createRef());
    }
    if (e.key === "Backspace" && e.target.textContent.length === 0) {
      // console.log("delete line");
      e.preventDefault();
      setCurAction({ type: "delete", index: index });
      setItemArr((prevState) => [
        ...prevState.slice(0, index),
        ...prevState.slice(index + 1),
      ]);
      elementsRef.current = itemArr
        .slice(0, itemArr.length - 1)
        .map(() => React.createRef());
    }
    // console.log(e);
    // if (e.keyCode === 38) {
    //   e.preventDefault();
    //   setCurAction({ type: "up", index: index });
    // }
    // if (e.keyCode === 40) {
    //   e.preventDefault();
    //   setCurAction({ type: "down", index: index });
    // }
  };

  // 當 memo 是空的時候，第一次點擊觸發
  const handleClick = (e) => {
    // console.log(e.currentTarget.firstChild);
    if (itemArr.length === 0 && editable) {
      setItemArr([{ text: "" }]);
      elementsRef.current.push(React.createRef());
      setCurAction({ type: "firstType", index: 0 });
    }
    if (setShowSaveBtn) {
      setShowSaveBtn(true);
    }
  };

  useEffect(() => {
    switch (curAction.type) {
      case "firstType":
        elementsRef.current[curAction.index].current.focus();
        break;
      case "enter":
        elementsRef.current[curAction.index + 1].current.focus();
        break;
      case "delete":
        if (curAction.index === 0) {
          setCarat(elementsRef.current[curAction.index]?.current);
        } else {
          setCarat(elementsRef.current[curAction.index - 1].current);
        }
        break;
      // case "up":
      //   setCarat(elementsRef.current[curAction.index - 1]?.current);
      // case "down":
      //   setCarat(elementsRef.current[curAction.index + 1]?.current);
      default:
        return;
    }
  }, [curAction, elementsRef]);

  useEffect(() => {
    if (itemList) {
      setItemArr(itemList);
    }
  }, [itemList]);

  return (
    <>
      <div
        className={cx("h-full w-full grow overflow-auto", {
          [`cursor-text`]: editable,
        })}
        onClick={handleClick}
        ref={listContainerRef}
      >
        {itemArr.map((item, index) => (
          <div
            key={index}
            className={cx(
              "relative flex w-full items-start justify-between px-4 xl:py-1.5 3xl:py-3",
              {
                [`bg-gradient-to-r from-primary-op20 to-success-op20`]:
                  pickId === item.id && !editable && item.has_note,
              },
            )}
            onClick={() => {
              setPickId(item.id);
              detailCb(item.id);
            }}
          >
            <div
              className={`me-5 mt-2.5 h-3 w-3 rounded-full ${bulletColor} shrink-0`}
            ></div>
            <div
              className={`grow overflow-auto whitespace-normal break-all text-clamp24 font-medium outline-0`}
              onBlur={(e) => handleItemChange(e, index)}
              onKeyDown={(e) => handleKeyDown(e, index)}
              contentEditable={editable}
              suppressContentEditableWarning={editable}
              id={`item-${index}`}
              ref={elementsRef.current[index]}
              data-doneat={item["done_at"] ?? null}
              data-note={item.has_note}
            >
              {item.text}
            </div>
            {item.has_note && !editable && (
              <AiOutlineEye className="shrink-0 stroke-black text-clamp24" />
            )}
            {item["done_at"] && (
              <div className="ms-1 self-center whitespace-nowrap text-clamp16 text-grey">
                {t("doneTime", { time: item["done_at"] })}
              </div>
            )}
          </div>
        ))}
      </div>
      <div className="absolute bottom-1 right-3">
        {editable && showSaveBtn && (
          <button
            onClick={() => {
              storeHandler(itemArr);
            }}
          >
            <BsCheckLg className="h-5 w-5" />
          </button>
        )}
      </div>
    </>
  );
};

export default ItemList;
