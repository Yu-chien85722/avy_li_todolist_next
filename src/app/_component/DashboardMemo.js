"use client";

import useSWR from "swr";
import { getData, putData } from "../api";
import { useMemo, useState } from "react";
import React from "react";
import ItemList from "./ItemList";
import { useTranslations } from "next-intl";
import { fetchDataErrHandler } from "../lib/global";
import moment from "moment";
import FailLoad from "@/app/_component/FailLoad";
import UserAuthHOC from "@/app/_component/HOC/UserAuthHOC";

function DashboardMemo({ session, status }) {
  const today = moment(new Date()).format("YYYY-MM-DD");
  // save button
  const [showSaveBtn, setShowSaveBtn] = useState(false);
  // intel
  const t = useTranslations("ItemList");
  // 一開始 session is undefined，因為正在 loading 中，loading 完成後
  // session 有值 => re-render => useSWR 就會得到 token 成功取得資料
  const { data, error, isLoading, mutate } = useSWR(
    session
      ? [`calendar/thing/${today.replace(/-/g, "/")}`, session.accessToken]
      : null,
    ([url, token]) => getData(url, token),
  );

  const storeHandler = (itemArr) => {
    // console.log("result", itemArr);
    const updateMemoList = { memo: itemArr.map((item) => item.text) };
    putData(
      `calendar/thing/${today}`,
      session.accessToken,
      JSON.stringify(updateMemoList),
    )
      .then((result) => {
        console.log(result);
        if (result) {
          setShowSaveBtn(false);
        }
      })
      .catch((error) => {
        mutate();
        fetchDataErrHandler(error, "save memo");
      });
  };

  // 將 memo 資料整理成 [{id:1, text:'sfsdf'}, {id:2, text:'sfsf'}]
  // console.log(data);
  const memoList = useMemo(() => {
    if (data) {
      let memos = Object.values(data)[0]?.memo ?? [];
      memos = memos.map((ele, index) => ({
        id: index + 1,
        text: ele,
      }));
      return memos;
    }
  }, [data]);

  if (status === "loading" || isLoading) {
    return;
  }
  if (error) {
    fetchDataErrHandler(error, "get memo");
  }

  // console.log(memoList);
  return (
    <div className="relative flex h-full w-full flex-col rounded-2xl bg-white px-5 pb-[2.438rem] pt-6 shadow dark:bg-black">
      <div className="text-clamp24 font-medium">{t("title.memo")}</div>
      {error && <FailLoad />}
      {!error && (
        <ItemList
          itemList={memoList}
          bulletColor="bg-primary"
          editable={true}
          storeHandler={storeHandler}
          showSaveBtn={showSaveBtn}
          setShowSaveBtn={setShowSaveBtn}
        />
      )}
    </div>
  );
}

export default UserAuthHOC(DashboardMemo);
const dummyData = [
  { id: 1, text: "官網首頁 RWD", note: true },
  {
    id: 2,
    text: "盤點設變盤點設變盤點設變盤點設變盤點設變盤點設變盤點設變盤點設變盤點設變",
    note: true,
  },
  { id: 3, text: "好市多買食材" },
  { id: 4, text: "好市多買食材" },
  { id: 5, text: "好市多買食材" },
  { id: 6, text: "好市多買食材" },
];
