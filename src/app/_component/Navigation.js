"use client";

import React from "react";
import routes from "../lib/routes";
import LinkCard from "./LinkCard";
import { usePathname } from "@/navigation";
import { useTranslations } from "next-intl";

const matchPath = (route, pattern) => {
  // console.log("pattern", pattern);
  // console.log("route", route);
  const regex = new RegExp(`^${pattern}(\/|$)`);
  // console.log("test", regex.test(route));
  return regex.test(route);
};

const Navigation = () => {
  const t = useTranslations("Navigation");
  let curPath = usePathname();

  return (
    <div className="flex w-full flex-col pl-4 xl:gap-6 3xl:gap-9">
      {routes.map((route) => {
        // console.log(route.route.length);
        return (
          <LinkCard
            key={route.name}
            icon={route.icon}
            name={t(route.name)}
            route={route.route}
            active={matchPath(curPath, route.route)}
          />
        );
      })}
    </div>
  );
};

export default React.memo(Navigation);
