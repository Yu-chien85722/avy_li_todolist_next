"use client";

import cx from "classnames";
import React from "react";
import { useTranslations } from "next-intl";

const FormLabelText = ({ title, sideTitleHide, htmlFor = "inputSelect" }) => {
  const t = useTranslations("ProjectModal");
  return (
    <label htmlFor={htmlFor} className="mb-2.5 text-clamp18">
      <span className="me-2">{title}</span>
      <span className={cx("text-primary", { hidden: sideTitleHide })}>
        {"(" + t("required") + ")"}
      </span>
    </label>
  );
};

export default React.memo(FormLabelText);
