"use client";

import cls from "classnames";
import { useParams } from "next/navigation";
import { useTransition } from "react";
import useDropdown from "./hooks/useDropdown";
import { useRouter, usePathname } from "@/navigation";
import { BsGlobe } from "react-icons/bs";
import { Children } from "react";

export default function LocaleSwitcherDropdown({ children, label }) {
  const { containerRef, open, onToggle } = useDropdown();
  const router = useRouter();
  const [isPending, startTransition] = useTransition();
  const pathname = usePathname();
  const params = useParams();
  // console.log("pathname", pathname);
  // console.log("params", params);

  function onSelectChange(event) {
    const nextLocale = event.target.dataset.lang;
    console.log(event.target);
    startTransition(() => {
      router.replace(
        // @ts-expect-error -- TypeScript will validate that only known `params`
        // are used in combination with a given `pathname`. Since the two will
        // always match for the current route, we can skip runtime checks.
        { pathname, params },
        { locale: nextLocale },
      );
    });
  }

  return (
    <label
      className={cls(
        "relative text-grey",
        isPending && "transition-opacity [&:disabled]:opacity-30",
      )}
      ref={containerRef}
    >
      <p className="sr-only">{label}</p>
      <button disabled={isPending} onClick={onToggle}>
        <BsGlobe className="h-6 w-6 fill-black dark:fill-white 3xl:h-8 3xl:w-8"></BsGlobe>
        <div
          className={cls(
            "dropMenu-container absolute -bottom-3.5 left-14 bg-white-200 dark:bg-black-800",
            { ["invisible"]: !open },
          )}
        >
          {Children.map(children, (child) => {
            return (
              <div onClick={onSelectChange} className="hover:bg-primary-op10">
                {child}
              </div>
            );
          })}
        </div>
      </button>
    </label>
  );
}
