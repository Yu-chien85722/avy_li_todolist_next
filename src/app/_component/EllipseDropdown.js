"use client";

import React from "react";
import { AiOutlineEllipsis } from "react-icons/ai";
import cls from "classnames";
import useDropdown from "./hooks/useDropdown";

/*
how to use 
1. open => dropdown open or not
2. dropdwon menu as children
3. container set position: relative
*/

const EllipseDropdown = ({ children }) => {
  const { containerRef, open, onToggle, closeDropdown } = useDropdown();
  return (
    <>
      <button className="ellipsis-btn" onClick={onToggle} ref={containerRef}>
        <AiOutlineEllipsis className="text-lg" />
      </button>
      <div onClick={closeDropdown} className={cls({ ["invisible"]: !open })}>
        {children}
      </div>
    </>
  );
};

export default React.memo(EllipseDropdown);
