"use client";

import { useState } from "react";
import ProjectCard from "./ProjectCard";
import React from "react";

export default function ProjectAccordion({ projectList }) {
  const [openProjectId, setOpenProjectId] = useState(
    projectList[0]?.id ?? null,
  );
  return (
    <div className="flex flex-col items-center justify-center xl:gap-6 3xl:gap-9">
      {projectList.length > 0 ? (
        projectList.map((project) => {
          return (
            <div
              key={project.id}
              className="w-full rounded-2xl bg-white shadow dark:bg-black"
            >
              <ProjectCard
                project={project}
                actionList={project.actionList}
                showFlag={true}
                operations={null}
                openProjectId={openProjectId}
                setOpenProjectId={setOpenProjectId}
              />
            </div>
          );
        })
      ) : (
        <div className="w-full rounded-2xl bg-white text-center text-clamp24 font-semibold shadow dark:bg-black xl:py-10 2xl:py-14">
          目前無專案
        </div>
      )}
    </div>
  );
}
