import { getProviders, signIn } from "next-auth/react";
import { getServerSession } from "next-auth/next";
import { authOptions } from "@/app/api/auth/[...nextauth]/auth";
import { redirect } from "next/navigation";
import SignInBtn from "./SignInBtn";

async function getProvider(searchParams) {
  console.log("getproviders");
  const session = await getServerSession(authOptions);
  console.log("session", session);
  const callbackUrl = searchParams.callbackUrl;

  // If the user is already logged in, redirect.
  // Note: Make sure not to redirect to the same page
  // To avoid an infinite loop!
  if (session) {
    // return { redirect: { destination: searchParams.callbackUrl } };
    redirect(`${callbackUrl}`);
  }

  const providers = await getProviders();
  console.log("get providers", providers);

  return providers ?? [];
}

export default async function SignInS({ searchParams }) {
  console.log("searchParams", searchParams);
  const providers = await getProvider(searchParams);
  console.log("providers", providers);

  return (
    <div className="rounded-2xl bg-white px-9 pb-[2.188rem] pt-[2.875rem] text-center dark:text-black">
      <div className="mb-3 text-clamp14 font-bold">登入 / 註冊</div>
      {Object.values(providers).map((provider) => {
        if (provider.id === "credentials") {
          return;
        }
        return <SignInBtn key={provider.name} provider={provider} />;
      })}
    </div>
  );
}
