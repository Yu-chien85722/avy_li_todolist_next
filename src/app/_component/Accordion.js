"use client";

import { useState, useEffect, createContext, useMemo } from "react";
import React from "react";

export const accrodionContext = createContext();

export default function Accordion({ children, initOpenId }) {
  const [openId, setOpenId] = useState(initOpenId);
  useEffect(() => setOpenId(initOpenId), [initOpenId]);
  const value = useMemo(() => ({ openId, setOpenId }), [openId]);
  return (
    <accrodionContext.Provider value={value}>
      <div className="flex flex-col items-center justify-center xl:gap-6 3xl:gap-9">
        {children}
      </div>
    </accrodionContext.Provider>
  );
}
