"use client";

import { useSession, signIn } from "next-auth/react";
import { useEffect } from "react";
import { AlertStatus } from "../alert/Alert";

const UserAuthHOC = (WrappedComponent) => {
  return function UserAuthHOC(props) {
    const { data: session, status } = useSession({
      required: true,
      onUnauthenticated() {
        // The user is not authenticated, handle it here.
        redirect("/signIn?callbackUrl=%2Fen");
      },
    });
    // console.log("UserAuthHoc", session);
    useEffect(() => {
      if (session?.error === "RefreshAccessTokenError") {
        AlertStatus("error", "RefreshAccessTokenError");
        signIn("/signIn?callbackUrl=%2Fen"); // Force sign in to hopefully resolve error
      }
    }, [session]);
    return (
      <WrappedComponent
        {...props}
        session={session}
        status={status}
      ></WrappedComponent>
    );
  };
};

export default UserAuthHOC;
