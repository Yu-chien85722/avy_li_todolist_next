import Swal from "sweetalert2";
import withReactContent from "sweetalert2-react-content";
import "./alert.scss";

/* 	MySwal 執行後
	1.會跳出 alert 視窗
	2.使用者做了XX事（click confirm or cancel btn or do nothing
	3. return Promise obj => then 接收，依據 user 行為的結果做不同的事
*/
const MySwal = withReactContent(Swal);

export const AlertStatus = (st, message) => {
  MySwal.fire({
    icon: st,
    html: `
    <p>${message}</p>
    `,
    confirmButtonText: "OK",
    buttonsStyling: false,
    allowOutsideClick: false,
    customClass: {
      confirmButton: "swal2-confirm-singleBtn",
      popup: "swal2-popup-customer",
      htmlContainer: "swal2-html-container-customer",
    },
  });
};

export const AlertStatusCb = (st, title, message, cb) => {
  MySwal.fire({
    icon: st,
    html: `
    <h4>${title}</h4>
    <p>${message}</p>
    `,
    confirmButtonText: "OK",
    buttonsStyling: false,
    allowOutsideClick: false,
    customClass: {
      confirmButton: "swal2-confirm-singleBtn",
      popup: "swal2-popup-customer",
      htmlContainer: "swal2-html-container-customer",
    },
  }).then((result) => {
    if (result.isConfirmed === true) {
      cb();
    }
  });
};

export const AlertText = (title, message, btnTxt) => {
  const swalOptions = {
    html: `
		<h4>${title}</h4>
		<p>${message}</p>
		`,
    confirmButtonText: btnTxt,
    buttonsStyling: false,
    allowOutsideClick: false,
    backdrop: true,
    customClass: {
      confirmButton: "swal2-confirm-singleBtn",
      popup: "swal2-popup-customer",
      htmlContainer: "swal2-html-container-customer",
    },
  };

  return MySwal.fire(swalOptions);
};

export const AlertTwoButton = (message, confirmTxt, cancelButtonText, cb) => {
  MySwal.fire({
    html: `
    <p>${message}</p>
    `,
    showCancelButton: true,
    confirmButtonText: confirmTxt,
    cancelButtonText: cancelButtonText,
    buttonsStyling: false,
    allowOutsideClick: false,
    reverseButtons: true,
    customClass: {
      confirmButton: "swal2-confirm-twoBtn",
      cancelButton: "swal2-cancel-twoBtn",
      popup: "swal2-popup-customer",
      htmlContainer: "swal2-html-container-customer",
    },
  }).then((result) => {
    if (result.isConfirmed === false) {
      cb();
    }
  });
};

export const AlertConfirmButton = (
  st,
  iconColor,
  title,
  message,
  confirmTxt,
  cancelButtonText,
  cb,
) => {
  MySwal.fire({
    icon: st,
    iconColor: iconColor,
    html: `
    <h5>${title}</h5>
    <p>${message}</p>
    `,
    showCancelButton: true,
    confirmButtonText: confirmTxt,
    cancelButtonText: cancelButtonText,
    buttonsStyling: false,
    allowOutsideClick: false,
    allowEscapeKey: false,
    reverseButtons: true,
    customClass: {
      confirmButton: "swal2-confirm-twoBtn",
      cancelButton: "swal2-cancel-twoBtn",
      popup: "swal2-popup-customer",
      htmlContainer: "swal2-html-container-customer",
    },
  }).then((result) => {
    if (result.isConfirmed === true) {
      cb();
    }
  });
};

export const AlertConfirmCancelButton = (
  title,
  message,
  confirmTxt,
  cancelButtonText,
  confirmCb,
  cancelCb,
) => {
  MySwal.fire({
    html: `
    <h5>${title}</h5>
    <p>${message}</p>
    `,
    showCancelButton: true,
    confirmButtonText: confirmTxt,
    cancelButtonText: cancelButtonText,
    buttonsStyling: false,
    allowOutsideClick: false,
    allowEscapeKey: false,
    reverseButtons: true,
    customClass: {
      confirmButton: "swal2-confirm-twoBtn",
      cancelButton: "swal2-cancel-twoBtn",
      popup: "swal2-popup-customer",
      htmlContainer: "swal2-html-container-customer",
    },
  }).then((result) => {
    if (result.isConfirmed === true) {
      confirmCb();
    } else if (result.isConfirmed === false) {
      cancelCb();
    }
  });
};

export const AlertLoadingCb = (icon, title, message, timer, cb) => {
  MySwal.fire({
    icon: icon,
    html: `
    <h4>${title}</h4>
    <p>${message}</p>
    `,
    buttonsStyling: false,
    allowOutsideClick: false,
    customClass: {
      popup: "swal2-popup-customer",
      htmlContainer: "swal2-html-container-customer",
    },
    timer: timer,
    timerProgressBar: true,
    didOpen: () => {
      MySwal.showLoading();
      // const b = MySwal.getHtmlContainer().querySelector('b');
      // timerInterval = setInterval(() => {
      // 	b.textContent = MySwal.getTimerLeft();
      // }, 100);
    },
    // willClose: () => {
    // 	clearInterval(timerInterval);
    // },
  }).then((result) => {
    /* Read more about handling dismissals below */
    if (result.dismiss === MySwal.DismissReason.timer) {
      cb();
    }
  });
};
