"use client";

import { useState } from "react";
import { signIn } from "next-auth/react";

/* for Credentials */
export default function SignInC({ searchParams }) {
  console.log("searchParams", searchParams);
  const [inputs, setInputs] = useState({ email: "", password: "" });

  const handleChange = (event) => {
    const name = event.target.name;
    const value = event.target.value;
    setInputs((values) => ({ ...values, [name]: value }));
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    await signIn("credentials", {
      email: inputs.email,
      password: inputs.password,
      callbackUrl: searchParams.callbackUrl ?? "/",
    });
  };

  return (
    <form className="space-y-6" onSubmit={handleSubmit}>
      <div>
        <label
          htmlFor="email"
          className="block text-sm font-medium leading-6 text-gray-900"
        >
          email
        </label>
        <div className="mt-2">
          <input
            id="email"
            name="email"
            type="text"
            autoComplete="off"
            required
            placeholder="test@example.com"
            value={inputs.email || ""}
            onChange={handleChange}
            className="block w-full rounded-md border-0 px-2 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
          />
        </div>
      </div>
      <div>
        <div className="flex items-center justify-between">
          <label
            htmlFor="password"
            className="block text-sm font-medium leading-6 text-gray-900"
          >
            Password
          </label>
        </div>
        <div className="mt-2">
          <input
            id="password"
            name="password"
            type="password"
            autoComplete="off"
            required
            placeholder="password"
            value={inputs.password || ""}
            onChange={handleChange}
            className="block w-full rounded-md border-0 px-2 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
          />
        </div>
      </div>
      <div>
        <button
          type="submit"
          className="flex w-full justify-center rounded-md bg-indigo-600 px-3 py-1.5 text-sm font-semibold leading-6 text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600"
        >
          Sign in
        </button>
      </div>
      {searchParams.error && (
        <p className="text-center capitalize text-red-600">sign in failed.</p>
      )}
    </form>
  );
}
