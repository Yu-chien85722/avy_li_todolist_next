"use client";

import React from "react";
import { useState, useEffect, useCallback } from "react";
import Modal from "./Modal";
import FormLabelText from "./FormText";
import { getData } from "@/app/api";
import {
  fetchDataErrHandler,
  REG_EXP_PROJECTNAME,
  isDateInPeriod,
} from "@/app/lib/global";
import { useTranslations } from "next-intl";
import { IoCalendarClearOutline } from "react-icons/io5";
import { IoWarningOutline } from "react-icons/io5";
import UserAuthHOC from "@/app/_component/HOC/UserAuthHOC";

const MissionModal = ({
  session,
  status,
  projectData,
  openType,
  missionId,
  postFormData,
  setOpen,
}) => {
  // intl
  const t = useTranslations("MissionModal");
  const acBtnT = useTranslations("ActionBtn");
  // hint modal open or not
  const [hintOpen, setHintOpen] = useState(false);
  // dirty modal open or not
  const [dirtyModalOpen, setDirtyModalOpen] = useState(false);
  // new mission data
  const [newMissionData, setNewMissionData] = useState({
    name: "",
    begin_date: "",
    end_date: "",
  });
  const [originalMission, setOriginalMission] = useState({
    name: "",
    begin_date: "",
    end_date: "",
  });
  // console.log("original", originalMission);
  // console.log("new", newMissionData);

  const handleInuptChange = (e, inputName) => {
    setNewMissionData((prevState) => {
      return {
        ...prevState,
        [inputName]: e.target.value,
      };
    });
  };

  const onError = (inputName, val) => {
    let haveErr = false;
    if (val === "") {
      setHintOpen("require");
      return true;
    }
    switch (inputName) {
      case "name":
        if (!REG_EXP_PROJECTNAME.test(val)) {
          setHintOpen("characters");
          haveErr = true;
        }
        break;
      case "begin_date":
        if (
          !isDateInPeriod(
            val,
            projectData["begin_date"].slice(0, 10),
            projectData["end_date"].slice(0, 10),
          )
        ) {
          setHintOpen("timeScope");
          haveErr = true;
        }
        break;
      case "end_date":
        if (
          !isDateInPeriod(
            val,
            projectData["begin_date"].slice(0, 10),
            projectData["end_date"].slice(0, 10),
          )
        ) {
          setHintOpen("timeScope");
          haveErr = true;
        } else if (
          new Date(newMissionData["begin_date"]).getTime() >
          new Date(val).getTime()
        ) {
          setHintOpen("endTimeErr");
          haveErr = true;
        }
        break;
      default:
        break;
    }
    return haveErr;
  };

  const onSubmit = () => {
    // const { name, begin_date, end_date, priority } = newMissionData;
    // console.log(newMissionData);
    for (const inputName in newMissionData) {
      if (onError(inputName, newMissionData[inputName])) {
        return;
      }
    }
    // console.log("all OK.");
    onCloseModal();
    postFormData(newMissionData);
  };
  const onCheckIsDirty = () => {
    let isDirty = false;
    if (JSON.stringify(originalMission) !== JSON.stringify(newMissionData)) {
      isDirty = true;
    }
    if (isDirty) {
      setDirtyModalOpen(true);
    } else {
      onCloseModal();
    }
  };

  const onCloseModal = () => {
    // close modal
    setOpen(false);
    // init modal content
    setHintOpen(false);
    setDirtyModalOpen(false);
    setNewMissionData({
      name: "",
      begin_date: "",
      end_date: "",
    });
    setOriginalMission({
      name: "",
      begin_date: "",
      end_date: "",
    });
  };

  const getSpecificMission = useCallback(() => {
    getData(`project/mission/${missionId}`, session.accessToken)
      .then((result) => {
        // console.log("single mission", result);
        const { name, begin_date, end_date } = result;
        // set mission data
        setOriginalMission({ name, begin_date, end_date });
        setNewMissionData({ name, begin_date, end_date });
      })
      .catch((err) => {
        fetchDataErrHandler(err, "get specific mission data");
      });
  }, [missionId, session]);

  // get specific mission data
  useEffect(() => {
    if (status === "authenticated" && openType === "update") {
      getSpecificMission();
    }
  }, [openType, status, missionId, getSpecificMission]);

  return (
    <>
      {/* main modal */}
      <Modal
        open={openType === "create" || openType === "update"}
        onClose={onCheckIsDirty}
        title={t("modalTitle", { operation: openType })}
      >
        <div className="modal-body xl:gap-6 3xl:gap-9">
          <div className="w-full px-11">
            <div className="flex w-full flex-col items-start justify-center xl:mb-5 3xl:mb-3">
              <FormLabelText
                title={t("name")}
                sideTitleHide={false}
                htmlFor="mission-name"
              />
              <input
                id="mission-name"
                name="name"
                type="text"
                className="form-input"
                onChange={(e) => {
                  handleInuptChange(e, "name");
                }}
                value={newMissionData.name}
              />
            </div>
            <div className="flex items-center justify-center gap-2">
              <div className="flex w-1/2 flex-col items-start justify-center">
                <FormLabelText
                  title={t("missionStartDate")}
                  sideTitleHide={false}
                  htmlFor="mission_begin_date"
                />
                <div className="relative float-left w-full">
                  <input
                    id="mission_begin_date"
                    name="begin_date"
                    type="date"
                    className="form-input"
                    onChange={(e) => {
                      handleInuptChange(e, "begin_date");
                    }}
                    value={newMissionData["begin_date"].slice(0, 10)}
                    min={projectData["begin_date"].slice(0, 10)}
                    max={projectData["end_date"].slice(0, 10)}
                  />
                  <button className="pointer-events-none absolute right-3 top-2.5 bg-white text-lg dark:bg-black">
                    <IoCalendarClearOutline className="text-primary" />
                  </button>
                </div>
              </div>
              <div className="flex w-1/2 flex-col items-start justify-center">
                <FormLabelText
                  title={t("missionEndDate")}
                  sideTitleHide={false}
                  htmlFor="mission_end_date"
                />
                <div className="relative float-left w-full">
                  <input
                    id="mission_end_date"
                    name="end_date"
                    type="date"
                    className="form-input"
                    onChange={(e) => {
                      handleInuptChange(e, "end_date");
                    }}
                    value={newMissionData["end_date"].slice(0, 10)}
                    min={projectData["begin_date"].slice(0, 10)}
                    max={projectData["end_date"].slice(0, 10)}
                  />
                  <button className="pointer-events-none absolute right-3 top-2.5 bg-white text-lg dark:bg-black">
                    <IoCalendarClearOutline className="text-primary" />
                  </button>
                </div>
              </div>
            </div>
          </div>
          <div className="flex gap-6">
            <button className="btn-border" onClick={onCheckIsDirty}>
              {acBtnT("cancel")}
            </button>
            <button className="btn-bg-full" onClick={onSubmit}>
              {acBtnT("save")}
            </button>
          </div>
        </div>
      </Modal>
      {/* hint modal */}
      <Modal
        open={hintOpen}
        onClose={() => {
          setHintOpen(false);
        }}
        title={t("hint.title")}
      >
        <div className="modal-body">
          <div className="text-center">
            <div className="mb-4 flex items-center justify-center">
              <IoWarningOutline className="h-[3.75rem] w-[3.75rem] text-danger" />
            </div>
            {hintOpen && (
              <>
                <div className="text-clamp20">
                  {t.rich("hint.type." + hintOpen, {
                    p: (chunks) => <p>{chunks}</p>,
                  })}
                </div>
              </>
            )}
          </div>
          <div className="flex gap-6">
            <button
              className="btn-bg-full"
              onClick={() => {
                setHintOpen(false);
              }}
            >
              {acBtnT("save")}
            </button>
          </div>
        </div>
      </Modal>
      {/* dirty modal */}
      <Modal
        open={dirtyModalOpen}
        onClose={() => {
          setDirtyModalOpen(false);
        }}
        title={t("dirtyDescrip.title")}
      >
        <div className="modal-body">
          <div className="text-center">
            <div className="mb-4 flex items-center justify-center">
              <IoWarningOutline className="h-[3.75rem] w-[3.75rem] text-danger" />
            </div>
            <div className="text-clamp20">{t("dirtyDescrip.descrip")}</div>
          </div>
          <div className="flex gap-6">
            <button
              className="btn-border"
              onClick={() => {
                setDirtyModalOpen(false);
              }}
            >
              {acBtnT("cancel")}
            </button>
            <button
              className="btn-bg-full"
              onClick={() => {
                onCloseModal();
              }}
            >
              {acBtnT("save")}
            </button>
          </div>
        </div>
      </Modal>
    </>
  );
};

export default React.memo(UserAuthHOC(MissionModal));
