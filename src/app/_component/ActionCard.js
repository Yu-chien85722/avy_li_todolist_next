"use client";
import React, { Children } from "react";
import { useTranslations } from "next-intl";

/*
itemData = obj = item 相關的所有資料
nameStyle = action name's style
checkHandler = 完成打勾的 handler
children = the custom element on the right
*/

const ActionCardR = ({ itemData, nameStyle, checkHandler, children }) => {
  return (
    <div className="flex h-full w-full items-center gap-4 rounded-2xl border border-grey xl:px-3.5 2xl:py-2 3xl:px-6 3xl:py-4">
      {checkHandler && (
        <div>
          <input
            id={itemData.id}
            type="checkbox"
            className="accent-primary xl:h-4 xl:w-4 3xl:h-6 3xl:w-6"
            onChange={() => {
              checkHandler(itemData.id);
            }}
          />
        </div>
      )}
      <div className={`grow truncate ${nameStyle}`} title={itemData.name}>
        {itemData.name}
      </div>
      {children}
    </div>
  );
};

export default React.memo(ActionCardR);
