"use client";

import React from "react";
import { useState, useEffect, useRef } from "react";
import Modal from "./Modal";
import FormLabelText from "./FormText";
import { getData } from "@/app/api";
import { fetchDataErrHandler, REG_EXP_PROJECTNAME } from "@/app/lib/global";
import tailwindConfig from "../../../tailwind.config";
import { useTranslations } from "next-intl";
import { FiFlag } from "react-icons/fi";
import { IoCalendarClearOutline } from "react-icons/io5";
import { IoWarningOutline } from "react-icons/io5";
import UserAuthHOC from "@/app/_component/HOC/UserAuthHOC";
import useDropdown from "./hooks/useDropdown";

const ProjectModal = ({
  session,
  status,
  openType,
  projectId,
  postFormData,
  setOpen,
}) => {
  // intl
  const t = useTranslations("ProjectModal");
  const acBtnT = useTranslations("ActionBtn");
  // for select priority
  const { containerRef, open, onToggle, closeDropdown } = useDropdown();
  // hint modal open or not
  const [hintOpen, setHintOpen] = useState(false);
  // dirty modal open or not
  const [dirtyModalOpen, setDirtyModalOpen] = useState(false);
  // new project data
  const [newProjectData, setNewProjectData] = useState({
    name: "",
    priority: 0,
    begin_date: "",
    end_date: "",
  });
  const [originalProject, setOriginalProject] = useState({
    name: "",
    priority: "",
    begin_date: "",
    end_date: "",
  });
  // console.log("original", originalProject);
  // console.log("new", newProjectData);

  const handleInuptChange = (e, inputName) => {
    setNewProjectData((prevState) => {
      return {
        ...prevState,
        [inputName]: e.target.value,
      };
    });
  };

  const handleSelectChange = (e) => {
    // console.log(e.currentTarget.dataset.priority);
    const newPriority = Number(e.currentTarget.dataset.priority);
    setNewProjectData((prevState) => {
      return {
        ...prevState,
        priority: newPriority,
      };
    });
  };

  const onError = (inputName, val) => {
    let haveErr = false;
    if (val === "") {
      setHintOpen("require");
      return true;
    }
    switch (inputName) {
      case "name":
        if (!REG_EXP_PROJECTNAME.test(val)) {
          setHintOpen("characters");
          haveErr = true;
        }
        break;
      case "end_date":
        if (
          new Date(newProjectData["begin_date"]).getTime() >
          new Date(val).getTime()
        ) {
          setHintOpen("endTimeErr");
          haveErr = true;
        }
        break;
      default:
        break;
    }
    return haveErr;
  };

  const onSubmit = () => {
    // const { name, begin_date, end_date, priority } = newProjectData;
    // console.log(newProjectData);
    for (const inputName in newProjectData) {
      if (onError(inputName, newProjectData[inputName])) {
        return;
      }
    }
    // console.log("all OK.");
    onCloseModal();
    postFormData(newProjectData);
  };

  const onCheckIsDirty = () => {
    let isDirty = false;
    if (JSON.stringify(originalProject) !== JSON.stringify(newProjectData)) {
      isDirty = true;
    }
    if (isDirty) {
      setDirtyModalOpen(true);
    } else {
      onCloseModal();
    }
  };

  const onCloseModal = () => {
    // close modal
    setOpen(false);
    // init modal content
    closeDropdown();
    setHintOpen(false);
    setDirtyModalOpen(false);
    setNewProjectData({
      name: "",
      priority: 0,
      begin_date: "",
      end_date: "",
    });
    setOriginalProject({
      name: "",
      priority: "",
      begin_date: "",
      end_date: "",
    });
  };

  const getSpecificProject = () => {
    getData(`project/${projectId}`, session.accessToken)
      .then((result) => {
        console.log("single project", result);
        const { name, priority, begin_date, end_date } = result;
        // set project data
        setOriginalProject({ name, priority, begin_date, end_date });
        setNewProjectData({ name, priority, begin_date, end_date });
      })
      .catch((err) => {
        fetchDataErrHandler(err, "get specific project data");
      });
  };

  // get specific project data
  useEffect(() => {
    if (status === "authenticated" && openType === "update") {
      getSpecificProject();
    }
  }, [openType, status, projectId]);

  return (
    <>
      {/* main modal */}
      <Modal
        open={openType === "create" || openType === "update"}
        onClose={onCheckIsDirty}
        title={t("modalTitle", { operation: openType })}
      >
        <div className="modal-body xl:gap-6 3xl:gap-9">
          <div className="w-full px-11">
            <div className="flex items-start justify-center gap-2 xl:mb-5 3xl:mb-3">
              <div className="flex w-8/12 flex-col items-start justify-center">
                <FormLabelText
                  title={t("name")}
                  sideTitleHide={false}
                  htmlFor="project-name"
                />
                <input
                  id="project-name"
                  name="name"
                  type="text"
                  className="form-input"
                  onChange={(e) => {
                    handleInuptChange(e, "name");
                  }}
                  value={newProjectData.name}
                />
              </div>
              <div className="flex w-4/12 flex-col items-start justify-center">
                <FormLabelText
                  title={t("priority")}
                  sideTitleHide={false}
                  htmlFor="priority-dropdown"
                />
                <div
                  id="priority-dropdown"
                  className="form-select relative cursor-pointer"
                  onClick={onToggle}
                  ref={containerRef}
                >
                  {newProjectData.priority === 2 && (
                    <div className="flex h-full items-center justify-start pl-2">
                      <FiFlag
                        className={`me-2 fill-danger stroke-black text-clamp20 dark:stroke-white`}
                      />
                      <span className="text-clamp18">P1</span>
                    </div>
                  )}
                  {newProjectData.priority === 1 && (
                    <div className="flex h-full items-center justify-start pl-2">
                      <FiFlag
                        className={`me-2 fill-primary stroke-black text-clamp20 dark:stroke-white`}
                      />
                      <span className="text-clamp18">P2</span>
                    </div>
                  )}
                  {newProjectData.priority === 0 && (
                    <div className="flex h-full items-center justify-start pl-2">
                      <FiFlag
                        className={`me-2 fill-success stroke-black text-clamp20 dark:stroke-white`}
                      />
                      <span className="text-clamp18">P3</span>
                    </div>
                  )}
                  <div
                    id="dropdownMenu"
                    className={`absolute left-0 top-[2.625rem] z-10 w-full cursor-pointer rounded-lg border border-grey-163 bg-white dark:bg-black ${
                      open ? "visible" : "invisible"
                    }`}
                  >
                    <div
                      className="flex items-center justify-start py-1 pl-4 hover:bg-primary-op10"
                      onClick={(e) => {
                        handleSelectChange(e);
                      }}
                      data-priority="2"
                    >
                      <FiFlag
                        className={`me-2 fill-danger stroke-black text-clamp20 dark:stroke-white`}
                      />
                      <span className="text-sm">P1</span>
                    </div>
                    <div
                      className="flex items-center justify-start py-1 pl-4 hover:bg-primary-op10"
                      onClick={(e) => {
                        handleSelectChange(e);
                      }}
                      data-priority="1"
                    >
                      <FiFlag
                        className={`me-2 fill-primary stroke-black text-clamp20 dark:stroke-white`}
                      />
                      <span className="text-sm">P2</span>
                    </div>
                    <div
                      className="flex items-center justify-start py-1 pl-4 hover:bg-primary-op10"
                      onClick={(e) => {
                        handleSelectChange(e);
                      }}
                      data-priority="0"
                    >
                      <FiFlag
                        className={`me-2 fill-success stroke-black text-clamp20 dark:stroke-white`}
                      />
                      <span className="text-sm">P3</span>
                    </div>
                  </div>
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="14"
                    height="11"
                    viewBox="0 0 14 11"
                    fill="none"
                    className="absolute right-3 top-3"
                  >
                    <path
                      d="M7.83467 9.73501C7.43943 10.334 6.56057 10.334 6.16533 9.73501L1.09502 2.05074C0.65633 1.38589 1.13315 0.5 1.9297 0.5L12.0703 0.5C12.8669 0.5 13.3437 1.38589 12.905 2.05074L7.83467 9.73501Z"
                      fill={tailwindConfig.theme.extend.colors.primary.DEFAULT}
                    />
                  </svg>
                </div>
              </div>
            </div>
            <div className="flex items-center justify-center gap-2">
              <div className="flex w-1/2 flex-col items-start justify-center">
                <FormLabelText
                  title={t("projectStartDate")}
                  sideTitleHide={false}
                  htmlFor="project_begin_date"
                />
                <div className="relative float-left w-full">
                  <input
                    id="project_begin_date"
                    name="begin_date"
                    type="date"
                    className="form-input"
                    onChange={(e) => {
                      handleInuptChange(e, "begin_date");
                    }}
                    value={newProjectData["begin_date"].slice(0, 10)}
                  />
                  <button className="pointer-events-none absolute right-3 top-2.5 bg-white text-lg dark:bg-black">
                    <IoCalendarClearOutline className="text-primary" />
                  </button>
                </div>
              </div>
              <div className="flex w-1/2 flex-col items-start justify-center">
                <FormLabelText
                  title={t("projectEndDate")}
                  sideTitleHide={false}
                  htmlFor="project_end_date"
                />
                <div className="relative float-left w-full">
                  <input
                    id="project_end_date"
                    name="end_date"
                    type="date"
                    className="form-input"
                    onChange={(e) => {
                      handleInuptChange(e, "end_date");
                    }}
                    value={newProjectData["end_date"].slice(0, 10)}
                  />
                  <button className="pointer-events-none absolute right-3 top-2.5 bg-white text-lg dark:bg-black">
                    <IoCalendarClearOutline className="text-primary" />
                  </button>
                </div>
              </div>
            </div>
          </div>
          <div className="flex gap-6">
            <button className="btn-border" onClick={onCheckIsDirty}>
              {acBtnT("cancel")}
            </button>
            <button className="btn-bg-full" onClick={onSubmit}>
              {acBtnT("save")}
            </button>
          </div>
        </div>
      </Modal>
      {/* Hint modal */}
      <Modal
        open={hintOpen}
        onClose={() => {
          setHintOpen(false);
        }}
        title={t("hint.title")}
      >
        <div className="modal-body">
          <div className="text-center">
            <div className="mb-4 flex items-center justify-center">
              <IoWarningOutline className="h-[3.75rem] w-[3.75rem] text-danger" />
            </div>
            {hintOpen && (
              <>
                <div className="text-clamp20">
                  {t.rich("hint.type." + hintOpen, {
                    p: (chunks) => <p>{chunks}</p>,
                  })}
                </div>
              </>
            )}
          </div>
          <div className="flex gap-6">
            <button
              className="btn-bg-full"
              onClick={() => {
                setHintOpen(false);
              }}
            >
              {acBtnT("save")}
            </button>
          </div>
        </div>
      </Modal>
      {/* dirty modal */}
      <Modal
        open={dirtyModalOpen}
        onClose={() => {
          setDirtyModalOpen(false);
        }}
        title={t("dirtyDescrip.title")}
      >
        <div className="modal-body">
          <div className="text-center">
            <div className="mb-4 flex items-center justify-center">
              <IoWarningOutline className="h-[3.75rem] w-[3.75rem] text-danger" />
            </div>
            <div className="text-clamp20">{t("dirtyDescrip.descrip")}</div>
          </div>
          <div className="flex gap-6">
            <button
              className="btn-border"
              onClick={() => {
                setDirtyModalOpen(false);
              }}
            >
              {acBtnT("cancel")}
            </button>
            <button
              className="btn-bg-full"
              onClick={() => {
                onCloseModal();
              }}
            >
              {acBtnT("save")}
            </button>
          </div>
        </div>
      </Modal>
    </>
  );
};

export default React.memo(UserAuthHOC(ProjectModal));
