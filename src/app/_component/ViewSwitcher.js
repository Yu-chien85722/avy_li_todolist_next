"use client";

import React from "react";
import { useState, useRef, useEffect } from "react";
import { ViewMode } from "gantt-task-react";
import tailwindConfig from "../../../tailwind.config";
import { useTranslations } from "next-intl";
import useDropdown from "./hooks/useDropdown";

const ViewSwitcher = ({ view, onViewModeChange }) => {
  // console.log(view);
  const t = useTranslations("ViewSwitcher");
  const { containerRef, open, onToggle } = useDropdown();
  return (
    <div
      id="view-mode-dropdown"
      className="relative flex w-[calc(100%*0.077+0.625rem)] cursor-pointer items-center justify-around rounded-lg border border-grey px-2 py-0.5 xl:mr-5 3xl:mr-9"
      onClick={onToggle}
      ref={containerRef}
    >
      {view === "Month" && (
        <div className="inline-block h-full">
          <span className="text-clamp24">{t("month")}</span>
        </div>
      )}
      {view === "Week" && (
        <div className="inline-block h-full">
          <span className="text-clamp24">{t("week")}</span>
        </div>
      )}
      {view === "Day" && (
        <div className="inline-block h-full">
          <span className="text-clamp24">{t("day")}</span>
        </div>
      )}
      <div
        id="dropdownMenu"
        className={`absolute left-0 top-[2.625rem] z-10 w-full cursor-pointer rounded-lg border border-grey-163 bg-white dark:bg-black ${
          open ? "visible" : "invisible"
        }`}
      >
        <div
          className="flex items-center justify-start py-1 pl-3 hover:bg-primary-op10"
          onClick={() => onViewModeChange(ViewMode.Month)}
        >
          <span className="text-sm">{t("month")}</span>
        </div>
        <div
          className="flex items-center justify-start py-1 pl-3 hover:bg-primary-op10"
          onClick={() => onViewModeChange(ViewMode.Week)}
        >
          <span className="text-sm">{t("week")}</span>
        </div>
        <div
          className="flex items-center justify-start py-1 pl-3 hover:bg-primary-op10"
          onClick={() => onViewModeChange(ViewMode.Day)}
        >
          <span className="text-sm">{t("day")}</span>
        </div>
      </div>
      <div>
        <svg
          xmlns="http://www.w3.org/2000/svg"
          width="14"
          height="11"
          viewBox="0 0 14 11"
          fill="none"
        >
          <path
            d="M7.83467 9.73501C7.43943 10.334 6.56057 10.334 6.16533 9.73501L1.09502 2.05074C0.65633 1.38589 1.13315 0.5 1.9297 0.5L12.0703 0.5C12.8669 0.5 13.3437 1.38589 12.905 2.05074L7.83467 9.73501Z"
            fill={tailwindConfig.theme.extend.colors.primary.DEFAULT}
          />
        </svg>
      </div>
    </div>
  );
};

export default React.memo(ViewSwitcher);
