"use client";

import React from "react";
import { signIn, signOut } from "next-auth/react";
import Button from "./Button";
import { useTranslations } from "next-intl";

const SignOutBtn = () => {
  const t = useTranslations("Navigation");
  return (
    <Button
      title={{
        className: "rounded-xl border-2 border-primary px-6 py-2 text-clamp20",
        text: t("logout"),
      }}
      clickHandler={() => {
        signOut();
      }}
    />
  );
};

export default React.memo(SignOutBtn);
