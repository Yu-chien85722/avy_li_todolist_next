"use client";

import useSWR from "swr";
import { getData, postData } from "../api";
import { useState } from "react";
// ProjectCard become client component because it import after "use client"
import ProjectCard from "./ProjectCard";
import UserAuthHOC from "./HOC/UserAuthHOC";
import React from "react";
import { fetchDataErrHandler } from "../lib/global";
import { Link } from "@/navigation";
import ProjectModal from "./ProjectModal";
import Button from "./Button";
import { useTranslations } from "next-intl";
import { HiPlus } from "react-icons/hi";
import Loading from "@/app/[locale]/loading";
import FailLoad from "./FailLoad";

function ProjectList({ session, status }) {
  const t = useTranslations("ProjectModal");

  // 一開始 session is undefined，因為正在 loading 中，loading 完成後
  // session 有值 => re-render => useSWR 就會得到 token 成功取得資料
  const { data, error, isLoading, mutate } = useSWR(
    session ? ["project/progress", session.accessToken] : null,
    ([url, token]) => getData(url, token),
  );

  if (status === "loading" || isLoading) {
    return <Loading />;
  }
  if (error) {
    fetchDataErrHandler(error, "load projects");
    return <FailLoad />;
  }
  // filter < 1 的 project 出來
  // console.log(data);
  let projectList = [];
  if (data?.length > 0) {
    projectList = data.filter((project) => {
      return Number(project.progress) < 1;
    });
  }
  return (
    <div className="flex flex-col items-center justify-center xl:gap-7 3xl:gap-9">
      {projectList.length > 0 ? (
        projectList.map((project) => {
          return (
            <Link
              href={`/project/${project.id}`}
              className="block w-full"
              key={project.id}
            >
              <div
                key={project.id}
                className="w-full rounded-2xl bg-white shadow dark:bg-black"
              >
                <ProjectCard
                  project={project}
                  showFlag={false}
                  operations={null}
                />
              </div>
            </Link>
          );
        })
      ) : (
        <Link href="/project" className="block w-full">
          <Button
            title={{
              text: t("modalTitle", { operation: "create" }),
              className: "create-btn w-full",
            }}
          >
            <div className="flex items-center justify-center rounded-full bg-primary xl:h-9 xl:w-9 2xl:h-10 2xl:w-10 3xl:h-[3.125rem] 3xl:w-[3.125rem]">
              <HiPlus className="fill-white text-clamp20" />
            </div>
          </Button>
        </Link>
      )}
    </div>
  );
}
export default UserAuthHOC(ProjectList);
