"use client";

import React from "react";
import { BsX } from "react-icons/bs";

const Modal = ({ open, onClose, children, title }) => {
  return (
    // backdrop
    <div
      className={`fixed inset-0 z-10 flex items-center justify-center transition-colors ${
        open ? "visible bg-black-op40" : "invisible"
      }`}
      onClick={onClose}
    >
      {/* modal */}
      {/* 避免 bubble 觸發 backdrop 的 click 事件 */}
      <div
        className={`rounded-2xl bg-white shadow transition-all dark:bg-black ${
          open ? "scale-100 opacity-100" : "scale-125 opacity-0"
        }`}
        onClick={(e) => e.stopPropagation()}
      >
        {/* header */}
        <div className="flex items-center justify-between rounded-t-2xl bg-primary text-clamp20 dark:text-black xl:px-6 xl:py-2 3xl:px-9 3xl:py-[1rem]">
          <div>{title}</div>
          <button>
            <BsX className="text-clamp36" onClick={onClose} />
          </button>
        </div>
        {/* body */}
        {children}
      </div>
    </div>
  );
};

export default React.memo(Modal);
