"use client";

import { useState, useEffect } from "react";
import { useTheme } from "next-themes";
import { IoSunnyOutline } from "react-icons/io5";
import { IoMoonOutline } from "react-icons/io5";
import useDropdown from "./hooks/useDropdown";
import clx from "classnames";

export default function ThemeColorSwitcher() {
  const [mounted, setMounted] = useState(false);
  const { containerRef, open, onToggle } = useDropdown();
  const { theme, setTheme } = useTheme();
  // const [darkThemeMq, setDarkThemeMq] = useState(false);

  // useEffect only runs on the client, so now we can safely show the UI
  useEffect(() => {
    // console.log("mount");
    setMounted(true);

    // const isDarkPrefer = window.matchMedia(
    //   "(prefers-color-scheme: dark)",
    // ).matches;
    // setDarkThemeMq(isDarkPrefer);
  }, []);

  if (!mounted) {
    return null;
  }

  return (
    <button onClick={onToggle} className="relative" ref={containerRef}>
      <IoSunnyOutline
        className={clx(
          "h-6 w-6 stroke-black dark:stroke-white 3xl:h-8 3xl:w-8",
          // {
          //   ["hidden"]: theme === "system" && darkThemeMq,
          // },
          {
            ["hidden"]: theme !== "light",
          },
        )}
      ></IoSunnyOutline>
      <IoMoonOutline
        className={clx(
          "h-6 w-6 stroke-black dark:stroke-white 3xl:h-8 3xl:w-8",
          // {
          //   ["hidden"]: theme === "system" && !darkThemeMq,
          // },
          {
            ["hidden"]: theme !== "dark",
          },
        )}
      ></IoMoonOutline>
      {open && (
        <div className="absolute -bottom-3.5 left-14 flex cursor-pointer flex-col items-stretch justify-start rounded-2xl bg-white-200 text-black dark:bg-black-800 dark:text-white">
          <div
            className={clx(
              "flex items-center justify-start gap-1 rounded-t-2xl px-3 py-2 hover:bg-primary-op10",
              {
                ["text-primary"]: theme === "light",
              },
            )}
            onClick={(e) => {
              setTheme("light");
            }}
          >
            <IoSunnyOutline
              className={clx("h-8 w-8 stroke-grey", {
                ["stroke-primary"]: theme === "light",
              })}
            ></IoSunnyOutline>
            light
          </div>
          <div
            className={clx(
              "flex items-center justify-start gap-1 rounded-b-2xl px-3 py-2 hover:bg-primary-op10",
              {
                ["text-primary"]: theme === "dark",
              },
            )}
            onClick={() => {
              setTheme("dark");
            }}
          >
            <IoMoonOutline
              className={clx("h-8 w-8 stroke-grey", {
                ["stroke-primary"]: theme === "dark",
              })}
            ></IoMoonOutline>
            dark
          </div>
          {/* <div
            className={clx(
              "flex items-center justify-start gap-1 rounded-b-2xl px-3 py-2 hover:bg-primary-op10",
              {
                ["text-primary"]: theme === "system",
              },
            )}
            onClick={() => {
              setTheme("system");
            }}
          >
            <IoDesktopOutline
              className={clx("h-8 w-8 stroke-grey", {
                ["stroke-primary"]: theme === "system",
              })}
            ></IoDesktopOutline>
            system
          </div> */}
        </div>
      )}
    </button>
  );
}
