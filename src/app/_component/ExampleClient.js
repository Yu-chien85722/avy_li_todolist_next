"use client";

import useSWR from "swr";
import { getData } from "../api";
import { signIn, useSession } from "next-auth/react";
import { useEffect } from "react";
import { redirect } from "next/navigation";
import { fetchDataErrHandler } from "../lib/global";
import { AlertStatus } from "./alert/Alert";

export default function ExampleClient() {
  const { data: session, status } = useSession({
    required: true,
    onUnauthenticated() {
      redirect("/api/auth/signin?callbackUrl=%2F");
    },
  });

  console.log("ExampleClient", session?.accessToken);

  // 一開始 session is undefined，因為正在 loading 中，loading 完成後，session 有值 => re-render => useSWR 就會得到 token 成功取得資料
  // console.log(session);
  const { data, error, isLoading } = useSWR(
    session
      ? [`${process.env.BACKEND_API_BASE}/api/project`, session.accessToken]
      : null,
    ([url, token]) => getData(url, token),
  );

  useEffect(() => {
    if (session?.error === "RefreshAccessTokenError") {
      AlertStatus("error", "RefreshAccessTokenError");
      signIn(); // Force sign in to hopefully resolve error
    }
  }, [session]);

  if (status === "loading") {
    return <div>Loading session...</div>;
  }
  if (isLoading) {
    return <div>loading data..</div>;
  }
  if (error) {
    fetchDataErrHandler(error, "load projects");
    return <div>failed to load projects.</div>;
  }
  return (
    <>
      {data.map((item) => {
        return <div key={item.id}>{item.name}</div>;
      })}
    </>
  );
}
