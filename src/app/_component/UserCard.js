"use client";

import Image from "next/legacy/image";
import Loading from "@/app/[locale]/loading";
import UserAuthHOC from "@/app/_component/HOC/UserAuthHOC";

function UserCard({ session, status }) {
  if (status === "loading") {
    return <Loading />;
  }

  return (
    <div className="flex flex-col items-center justify-center xl:gap-4 2xl:gap-5 3xl:gap-6">
      <div className="relative flex items-center justify-center rounded-full bg-grey-100 xl:h-[7.5rem] xl:w-[7.5rem] 2xl:h-[10rem] 2xl:w-[10rem] 3xl:h-[12.5rem] 3xl:w-[12.5rem]">
        {/* need modify next.config.js images remotePatterns */}
        {
          <Image
            src={session?.user?.image ?? "https://via.placeholder.com/150"}
            alt="Avatar"
            priority
            layout="fill"
            objectFit="cover"
            style={{ borderRadius: "100%" }}
          />
        }
      </div>
      <div className="flex items-center justify-center text-clamp24 font-medium">
        {session?.user?.name ?? "user name"}
      </div>
    </div>
  );
}

export default UserAuthHOC(UserCard);
