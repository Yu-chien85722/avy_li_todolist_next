"use client";

import { useState, useEffect, useRef } from "react";

const useDropdown = () => {
  // dropdown menu open or close
  const [open, setOpen] = useState(false);

  // dropdown container
  const containerRef = useRef(null);

  const onToggle = (e) => {
    // console.log(e);
    e.stopPropagation();
    setOpen((prevState) => !prevState);
  };

  const handleClickOutside = (e) => {
    // console.log(e.target);
    if (containerRef.current && !containerRef.current.contains(e.target)) {
      setOpen(false);
    }
  };

  const closeDropdown = (e) => {
    if (e) {
      e.stopPropagation();
    }
    setOpen(false);
  };

  useEffect(() => {
    document.addEventListener("click", handleClickOutside);
    return () => {
      document.removeEventListener("click", handleClickOutside);
    };
  });

  return { containerRef, open, onToggle, closeDropdown };
};

export default useDropdown;
