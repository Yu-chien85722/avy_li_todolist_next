import failureLoadImg from "@/app/images/failure.png";
import Image from "next/legacy/image";

export default function FailLoad() {
  return (
    <div className="flex h-full w-full flex-col items-center justify-center text-clamp24 text-grey">
      <Image src={failureLoadImg} alt="fail image" priority />
      <div>無法獲得資料</div>
    </div>
  );
}
