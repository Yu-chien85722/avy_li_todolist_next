"use client";
import { signIn, signOut } from "next-auth/react";
import React from "react";

const Button = ({ title, clickHandler, children }) => {
  return (
    <div
      className={`flex cursor-pointer items-center justify-center xl:gap-2 3xl:gap-4 ${title.className}`}
      onClick={clickHandler}
    >
      {children}
      <div>{title.text}</div>
    </div>
  );
};

export default React.memo(Button);
