"use client";
import React from "react";
import { AiOutlineEdit } from "react-icons/ai";
import { useTranslations } from "next-intl";

/*
mode = toDoList => 在 to do list 的 action => 未完成
     = done => 在 done list 的 action => 已完成
     = shouldDo => 在 project list 的 action => 未完成，但沒有在 to do list
itemData = obj = item 相關的所有資料
checkHandler = 完成打勾的 handler
editHandler = 完成後可編輯的 handler
*/

const ActionCard = ({ mode, itemData, checkHandler, editHandler }) => {
  const t = useTranslations("ActionCard");
  return (
    <div className="flex h-full w-full items-center gap-4 rounded-2xl border border-grey xl:px-3.5 2xl:py-2 3xl:px-6 3xl:py-4">
      {checkHandler && (
        <div>
          <input
            id={itemData.id}
            type="checkbox"
            className="accent-primary xl:h-4 xl:w-4 3xl:h-6 3xl:w-6"
            onChange={(e) => {
              checkHandler(itemData.id);
            }}
          />
        </div>
      )}
      <div
        className={`grow truncate ${
          mode === "toDoList" ? "text-clamp24 font-semibold" : "text-clamp20"
        }`}
        title={itemData.name}
      >
        {itemData.name}
      </div>
      {mode === "done" ? (
        <button
          onClick={(e) => {
            editHandler(itemData.id);
          }}
          id={itemData.id}
          className="w-fit"
        >
          <AiOutlineEdit className="me-2 stroke-black text-clamp24 dark:stroke-white" />
        </button>
      ) : (
        <div className="flex w-3/12 flex-col items-end justify-center">
          {mode === "toDoList" && (
            <div
              className="w-full truncate text-end text-clamp16 font-bold text-grey"
              title={itemData.root.name}
            >
              {itemData.root.name}
            </div>
          )}
          <div
            className={`whitespace-nowrap ${
              mode === "toDoList" ? "text-clamp18" : "text-sm"
            } ${itemData.day <= 0 ? "" : "text-danger"}`}
          >
            {t("days", {
              dayDiff: itemData.day <= 0 ? "moreThan" : "cross",
              day: Math.abs(itemData.day),
            })}
          </div>
        </div>
      )}
    </div>
  );
};

export default React.memo(ActionCard);
