import { withAuth } from "next-auth/middleware";
// import createIntlMiddleware from "next-intl/middleware";
import createMiddleware from "next-intl/middleware";
import { NextRequest } from "next/server";
import { locales, localePrefix, pathnames } from "./app/lib/global";

const publicPages = ["/api/auth/signin"];
// create intl middleware (i18n)
const intlMiddleware = createMiddleware({
  defaultLocale: "ch",
  locales,
  pathnames,
  localePrefix,
});

// create auth middleware (NextAuth)
const authMiddleware = withAuth(
  // Note that this callback is only invoked if
  // the `authorized` callback has returned `true`
  // and not for pages listed in `pages`.
  function onSuccess(req) {
    return intlMiddleware(req);
  },
  {
    callbacks: {
      authorized: ({ token }) => token != null,
    },
    pages: {
      // signIn: "/api/auth/signin",
      signIn: "/signIn",
    },
  },
);

export default function middleware(req: NextRequest) {
  const publicPathnameRegex = RegExp(
    `^(/(${locales.join("|")}))?(${publicPages
      .flatMap((p) => (p === "/" ? ["", "/"] : p))
      .join("|")})/?$`,
    "i",
  );
  // check if the route is public
  const isPublicPage = publicPathnameRegex.test(req.nextUrl.pathname);

  // if public => no authentication by NextAuth, if private, need pass the authenticaton by NextAuth
  if (isPublicPage) {
    return intlMiddleware(req);
  } else {
    return (authMiddleware as any)(req);
  }

  // easy to test
  // return intlMiddleware(req);
}

// match 以下的 route 皆須經過這個 middleware 判定
// route: /signIn 不經過這個 middleware 判定 => 不經 intl，也不需要身分驗證
export const config = {
  matcher: ["/(ch/en)/:path*", "/((?!api|_next|.*\\..*|signIn).*)"],
  runtime: "experimental-edge", // for Edge API Routes only
  unstable_allowDynamic: [
    "src/app/_component/alert/Alert.js", // allows a single file
    "/src/app/lib/global.js", // allows a single file
    "/node_modules/sweetalert2/dist/sweetalert2.all.js", // use a glob to allow anything in the function-bind 3rd party module
  ],
};
